//
//  DDSAnalytics.swift
//  TaErrado
//
//  Created by Gilson Gil on 10/9/15.
//  Copyright © 2015 doisdoissete. All rights reserved.
//

import Foundation
import Crashlytics

enum AnalyticsType {
  case Answers, GoogleAnalytics
}

final class DDSAnalytics {
  static private let sharedAnalytics = DDSAnalytics(types: [])
  private var types: [AnalyticsType] {
    didSet {
      for type in types {
        switch type {
        case .Answers:
          break
        case .GoogleAnalytics:
          setupGoogleAnalytics()
        }
      }
    }
  }
  
  init(types: [AnalyticsType]) {
    self.types = types
  }
  
  static func setupWithTypes(types: [AnalyticsType]) {
#if RELEASE
    sharedAnalytics.setupWithTypes(types)
#endif
  }
  
  private func setupWithTypes(types: [AnalyticsType]) {
    self.types = types
  }
  
  // MARK: Track Sign Up
  static func trackSignUpWithMethod(method: String, success: Bool, customAttributes: [String: AnyObject]?) {
    #if RELEASE
      sharedAnalytics.trackSignUpWithMethod(method, success: success, customAttributes: customAttributes)
    #endif
  }
  
  private func trackSignUpWithMethod(method: String, success: Bool, customAttributes: [String: AnyObject]?) {
    for type in types {
      switch type {
      case .Answers:
        trackAnswersSignUpWithMethod(method, success: success, customAttributes: customAttributes)
      case .GoogleAnalytics:
        trackGoogleAnalyticsSignupWithMethod(method, success: success, customAttributes: customAttributes)
      }
    }
  }
  
  // MARK: Track Login
  static func trackLoginWithMethod(method: String, success: Bool, customAttributes: [String: AnyObject]?) {
    #if RELEASE
      sharedAnalytics.trackLoginWithMethod(method, success: success, customAttributes: customAttributes)
    #endif
  }
  
  private func trackLoginWithMethod(method: String, success: Bool, customAttributes: [String: AnyObject]?) {
    for type in types {
      switch type {
      case .Answers:
        trackAnswersLoginWithMethod(method, success: success, customAttributes: customAttributes)
      case .GoogleAnalytics:
        trackGoogleAnalyticsLoginWithMethod(method, success: success, customAttributes: customAttributes)
      }
    }
  }
  
  // MARK: Set UserId
  static func setUserId(userId: String) {
#if RELEASE
    sharedAnalytics.setUserId(userId)
#endif
  }
  
  private func setUserId(userId: String) {
    for type in types {
      switch type {
      case .Answers:
        break
      case .GoogleAnalytics:
        setGoogleAnalyticsUserId(userId)
      }
    }
  }
  
  // MARK: Track Screen
  static func trackScreen(screenName: String, withContentId contentId: String?) {
    #if RELEASE
      sharedAnalytics.trackScreen(screenName, withContentId: contentId)
    #endif
  }
  
  private func trackScreen(screenName: String, withContentId contentId: String?) {
    for type in types {
      switch type {
      case .Answers:
        trackAnswersScreen(screenName, withContentId: contentId)
      case .GoogleAnalytics:
        trackGoogleAnalyticsScreen(screenName, withContentId: contentId)
      }
    }
  }
  
  // MARK: Track Event
  static func trackEvent(eventName: String, category: String?, label: String?, value: Int?, customAttributes: [String: AnyObject]?) {
#if RELEASE
    sharedAnalytics.trackEvent(eventName, category: category, label: label, value: value, customAttributes: customAttributes)
#endif
  }
  
  private func trackEvent(eventName: String, category: String?, label: String?, value: Int?, customAttributes: [String: AnyObject]?) {
    for type in types {
      switch type {
      case .Answers:
        trackAnswersEvent(eventName, customAttributes: customAttributes)
      case .GoogleAnalytics:
        trackGoogleAnalyticsEvent(eventName, category: category, label: label, value: value)
      }
    }
  }
  
  // MARK: Track Share
  static func trackShareWithNetwork(network: String, action: String, target: String) {
#if RELEASE
    sharedAnalytics.trackShareWithNetwork(network, action: action, target: target)
#endif
  }
  
  private func trackShareWithNetwork(network: String, action: String, target: String) {
    for type in types {
      switch type {
      case .Answers:
        trackAnswersShareWithNetwork(network, action: action, target: target)
      case .GoogleAnalytics:
        trackGoogleAnalyticsShareWithNetwork(network, action: action, target: target)
      }
    }
  }
}

// MARK: Answers
private extension DDSAnalytics {
  func trackAnswersSignUpWithMethod(method: String, success: Bool, customAttributes: [String: AnyObject]?) {
    Answers.logSignUpWithMethod(method, success: success, customAttributes: customAttributes)
  }
  
  func trackAnswersLoginWithMethod(method: String, success: Bool, customAttributes: [String: AnyObject]?) {
    Answers.logLoginWithMethod(method, success: success, customAttributes: customAttributes)
  }
  
  func trackAnswersScreen(screenName: String, withContentId contentId: String?) {
    Answers.logContentViewWithName(screenName, contentType: "Screen", contentId: contentId, customAttributes: nil)
  }
  
  func trackAnswersEvent(eventName: String, customAttributes: [String: AnyObject]?) {
    Answers.logCustomEventWithName(eventName, customAttributes: customAttributes)
  }
  
  func trackAnswersShareWithNetwork(network: String, action: String, target: String) {
    Answers.logShareWithMethod(network, contentName: action, contentType: nil, contentId: target, customAttributes: nil)
  }
}

// MARK: Google Analytics
private extension DDSAnalytics {
  func setupGoogleAnalytics() {
    let gai = GAI.sharedInstance()
    gai.trackUncaughtExceptions = false
  }
  
  func setGoogleAnalyticsUserId(userId: String) {
    let tracker = GAI.sharedInstance().defaultTracker
    tracker.set("&uid", value: userId)
  }
  
  func trackGoogleAnalyticsSignupWithMethod(method: String, success: Bool, customAttributes: [String: AnyObject]?) {
    trackGoogleAnalyticsEvent("SignUp", category: "UX", label: method, value: nil)
  }
  
  func trackGoogleAnalyticsLoginWithMethod(method: String, success: Bool, customAttributes: [String: AnyObject]?) {
    trackGoogleAnalyticsEvent("Login", category: "UX", label: method, value: nil)
  }
  
  func trackGoogleAnalyticsScreen(screenName: String, withContentId contentId: String?) {
    let tracker = GAI.sharedInstance().defaultTracker
    tracker.set(kGAIScreenName, value: screenName)
    if let contentId = contentId {
      tracker.set("id", value: contentId)
    }
    
    let builder = GAIDictionaryBuilder.createScreenView()
    tracker.send(builder.build() as [NSObject: AnyObject])
  }
  
  func trackGoogleAnalyticsEvent(eventName: String, category: String?, label: String?, value: Int?) {
    let tracker = GAI.sharedInstance().defaultTracker
    let builder = GAIDictionaryBuilder.createEventWithCategory(category, action: eventName, label: label, value: value)
    tracker.send(builder.build() as [NSObject: AnyObject])
  }
  
  func trackGoogleAnalyticsShareWithNetwork(network: String, action: String, target: String) {
    let tracker = GAI.sharedInstance().defaultTracker
    let builder = GAIDictionaryBuilder.createSocialWithNetwork(network, action: action, target: target)
    tracker.send(builder.build() as [NSObject : AnyObject])
  }
}
