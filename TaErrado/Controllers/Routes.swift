//
//  Routes.swift
//  TaErrado
//
//  Created by Gilson Gil on 9/30/15.
//  Copyright © 2015 doisdoissete. All rights reserved.
//

import Foundation
import JLRoutes

struct Routes {
  static let PushRoute = "PushRoute"
  static let PushRouteURL = NSURL(string: "taerrado://\(PushRoute)")!
  
  static func addRoutes() {
    JLRoutes.globalRoutes().addRoute("password/:token") { parameters -> Bool in
      guard let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate, token = parameters["token"] as? String else {
    
        return false
      } 
      appDelegate.goToNewPassword(token)
      
      return true
    }
    
    JLRoutes.globalRoutes().addRoute(PushRoute) { parameters -> Bool in
      guard let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate else {
        
        return false
      }
      appDelegate.goToProfile()
      
      return true
    }
    
    JLRoutes.globalRoutes().addRoute("com.doisdoissete.TaErrado.UploadApplicationShortcut") { _ -> Bool in
      guard let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate else {
      
        return false
      }
      if !(appDelegate.window?.rootViewController is NavigationController) {
        appDelegate.goToHome()
      }
      guard let navigationController = appDelegate.window?.rootViewController as? NavigationController else {
        
        return false
      }
      
      navigationController.goToCamera()
      
      return true
    }
  }
}
