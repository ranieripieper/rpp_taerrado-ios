//
//  PushNotification.swift
//  TaErrado
//
//  Created by Gilson Gil on 10/21/15.
//  Copyright © 2015 doisdoissete. All rights reserved.
//

import Foundation
import Alamofire
import Parse

private let pushTokenKey = "PushTokenKey"

struct PushNotification {
  static func registerForRemoteNotifications() {
    let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
    UIApplication.sharedApplication().registerUserNotificationSettings(settings)
    UIApplication.sharedApplication().registerForRemoteNotifications()
  }
  
  static func registerToken(token: NSData) {
    let installation = PFInstallation.currentInstallation()
    let oldToken = installation.deviceToken
    installation.setDeviceTokenFromData(token)
    installation.saveInBackground()
    
    guard (persistedToken() == nil || persistedToken() != oldToken), let _ = NetworkManager.Router.authToken, deviceToken = installation.deviceToken else {
      
      return
    }
    let params = ["user": ["device_token": deviceToken, "platform": "ios"]]
    Manager.sharedInstance.request(NetworkManager.Router.EditProfile(params))
  }
  
  static func persistedToken() -> String? {
    
    return PFInstallation.currentInstallation().deviceToken
  }
}
