//
//  MapViewDelegate.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/16/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewDelegateDelegate {
  func regionDidChange(center: CLLocationCoordinate2D)
  func goToOffence(offence: Offence, fromOffenceView offenceView: OffenceView)
}

class MapViewDelegate: NSObject {
  @IBOutlet weak var mapView: MKMapView!
  
  var offenceView: OffenceView?
  var selectedOffence: Offence?
  var selectedOffenceAnnotation: OffenceAnnotation?
  var delegate: MapViewDelegateDelegate?
  
  lazy var dateFormatter: NSDateFormatter = {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd/MM/yy - HH:mm"
    dateFormatter.locale = NSLocale(localeIdentifier: "pt-BR")
    return dateFormatter
    }()
}

extension MapViewDelegate: MKMapViewDelegate {
  func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
    if let offence = annotation as? Offence {
      var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("Offence")
      if annotationView == nil {
        annotationView = MKAnnotationView(annotation: offence, reuseIdentifier: "Offence")
        annotationView?.enabled = true
        annotationView?.image = UIImage(named: "icn_pin")
      } else {
        annotationView?.annotation = offence
      }
      if offenceView != nil {
        mapView.bringSubviewToFront(offenceView!)
      }
      return annotationView
    } else if let offenceAnnotation = annotation as? OffenceAnnotation {
      offenceView = mapView.dequeueReusableAnnotationViewWithIdentifier("OffenceAnnotation") as! OffenceView!
      if offenceView == nil {
        offenceView = OffenceView(annotation: offenceAnnotation, reuseIdentifier: "OffenceAnnotation")
        offenceView!.frame = CGRect(x: 0, y: 0, width: 350, height: 180)
        offenceView!.enabled = true
      } else {
        offenceView!.annotation = offenceAnnotation
      }
      offenceView!.configureWithOffence(offenceAnnotation.offence, dateFormatter: dateFormatter)
      offenceView!.delegate = self
      mapView.bringSubviewToFront(offenceView!)
      offenceView?.addView()
      return offenceView!
    }
    return nil
  }
  
  func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
    if let offence = view.annotation as? Offence {
      if selectedOffence != offence {
        if let selectedOffenceAnnotation = selectedOffenceAnnotation {
          mapView.removeAnnotation(selectedOffenceAnnotation)
          self.selectedOffenceAnnotation = nil
        }
        selectedOffence = offence
        selectedOffenceAnnotation = OffenceAnnotation(offence: offence)
        mapView.addAnnotation(selectedOffenceAnnotation!)
      }
    }
  }
  
  func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView) {
    if view.annotation is Offence {
      if selectedOffenceAnnotation != nil && offenceView != nil {
        if offenceView!.superview != nil {
          offenceView!.superview!.bringSubviewToFront(offenceView!)
        }
        offenceView!.removeView()
      }
    }
  }
  
  func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
    if offenceView != nil && offenceView!.superview != nil {
      offenceView!.superview!.bringSubviewToFront(offenceView!)
    }
    delegate?.regionDidChange(mapView.centerCoordinate)
  }
  
  func mapView(mapView: MKMapView, didAddAnnotationViews views: [MKAnnotationView]) {
    if offenceView != nil && offenceView!.superview != nil {
      offenceView!.superview!.bringSubviewToFront(offenceView!)
    }
  }
}

extension MapViewDelegate: OffenceViewDelegate {
  func didRemove() {
    if selectedOffence != nil {
      mapView.deselectAnnotation(selectedOffence, animated: false)
    }
    if let selectedOffenceAnnotation = selectedOffenceAnnotation {
      mapView.removeAnnotation(selectedOffenceAnnotation)
    }
    selectedOffence = nil
    selectedOffenceAnnotation = nil
    offenceView = nil
  }
  
  func goToOffence(offence: Offence, fromOffenceView offenceView: OffenceView) {
    delegate?.goToOffence(offence, fromOffenceView: offenceView)
  }
}
