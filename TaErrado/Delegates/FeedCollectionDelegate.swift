//
//  FeedCollectionDelegate.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol FeedCollectionDelegateDelegate {
  func didScroll(scrollView: UIScrollView)
  func didReachEnd()
}

class FeedCollectionDelegate: NSObject {
  @IBOutlet weak var feedViewController: FeedViewController? {
    didSet {
      delegate = feedViewController
    }
  }
  @IBOutlet weak var profileViewController: ProfileViewController? {
    didSet {
      delegate = profileViewController
    }
  }
  
  var delegate: FeedCollectionDelegateDelegate?
  
  lazy var dateFormatter: NSDateFormatter = {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd/MM/yy - HH:mm"
    dateFormatter.locale = NSLocale(localeIdentifier: "pt-BR")
    return dateFormatter
  }()
}

extension FeedCollectionDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if let feedViewController = feedViewController {
      return feedViewController.offences.count
    } else {
      return profileViewController!.offences.count
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("FeedCell", forIndexPath: indexPath) as! FeedCell
    if let feedViewController = feedViewController {
      cell.configureWithOffence(feedViewController.offences[indexPath.row], dateFormatter: dateFormatter, fromProfile: false)
      cell.reportCallback = {
        self.feedViewController?.reportCell($0)
      }
      cell.swearCallback = {
        self.feedViewController?.swearCell($0, on: $1)
      }
      cell.goToUserCallback = {
        self.feedViewController?.goToUserCell($0)
      }
    } else {
      cell.configureWithOffence(profileViewController!.offences[indexPath.row], dateFormatter: dateFormatter, fromProfile: true)
      cell.reportCallback = {
        self.profileViewController?.reportCell($0)
      }
      cell.swearCallback = {
        self.profileViewController?.swearCell($0, on: $1)
      }
      cell.goToUserCallback = {
        self.profileViewController?.goToUserCell($0)
      }
    }
    return cell
  }
}

extension FeedCollectionDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    delegate?.didScroll(scrollView)
    if scrollView.contentOffset.y > scrollView.contentSize.height - 2 * scrollView.bounds.height {
      delegate?.didReachEnd()
    }
  }
}
