//
//  AdMob.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation
import AdSupport

struct AdMob {
//  static let unitId = "ca-app-pub-3940256099942544/2934735716" // test
  static let unitId = "ca-app-pub-8596943581095511/9570482984" // prod
  
  static func request() -> GADRequest {
    let request = GADRequest()
//    let string = ASIdentifierManager.sharedManager().advertisingIdentifier.UUIDString
//    request.testDevices = [string]
    if let gender = NSUserDefaults.standardUserDefaults().objectForKey("Gender") as? String {
      if gender == "male" {
        request.gender = .Male
      } else if gender == "female" {
        request.gender = .Female
      } else {
        request.gender = .Unknown
      }
    }
    if LocationManager.latitude != 0 && LocationManager.longitude != 0 {
      request.setLocationWithLatitude(CGFloat(LocationManager.latitude), longitude: CGFloat(LocationManager.longitude), accuracy: CGFloat(LocationManager.sharedInstance.desiredAccuracy))
    }
    return request
  }
}
