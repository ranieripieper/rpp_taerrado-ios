//
//  User.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

public final class User: NSObject, NSCoding {
  let id: Int
  let admin: Bool
  let authToken: String?
  let createdAt: NSDate
  let email: String?
  let facebookId: String
  let gender: String?
  let name: String?
  let picturesCount: Int
  let swearings: Int
  var avatarURL: String?
  var superuser = false
  
  init(_ API: [String: AnyObject], dateFormatter: NSDateFormatter?) {
    let df: NSDateFormatter
    if dateFormatter == nil {
      df = NSDateFormatter()
      df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"
    } else {
      df = dateFormatter!
    }
    id = API["id"] as? Int ?? 0
    admin = API["admin"] as? Bool ?? false
    authToken = API["auth_token"] as? String
    createdAt = df.dateFromString(API["created_at"] as? String ?? "") ?? NSDate(timeIntervalSince1970: 0)
    email = API["email"] as? String
    facebookId = API["fb_id"] as? String ?? ""
    gender = API["gender"] as? String
    name = API["name"] as? String
    picturesCount = API["posts_count"] as? Int ?? 0
    swearings = API["curses_count"] as? Int ?? 0
    avatarURL = API["profile_image"] as? String ?? ""
    if facebookId == "10206608393278636" {
      superuser = true
    } else {
      superuser = API["superuser"] as? Bool ?? false
    }
  }
  
  init(id: Int, admin: Bool, authToken: String?, createdAt: NSDate, email: String?, facebookId: String, gender: String?, name: String?, picturesCount: Int, swearings: Int, avatarURL: String?, superuser: Bool) {
    self.id = id
    self.admin = admin
    self.authToken = authToken
    self.createdAt = createdAt
    self.email = email
    self.facebookId = facebookId
    self.gender = gender
    self.name = name
    self.picturesCount = picturesCount
    self.swearings = swearings
    self.avatarURL = avatarURL
    self.superuser = superuser
  }
  
  required convenience public init?(coder decoder: NSCoder) {
    guard let createdAt = decoder.decodeObjectForKey("createdAt") as? NSDate,
      facebookId = decoder.decodeObjectForKey("facebookId") as? String
      else {
        
        return nil
    }
    
    self.init(
      id: decoder.decodeIntegerForKey("id"),
      admin: decoder.decodeBoolForKey("admin"),
      authToken: decoder.decodeObjectForKey("authToken") as? String,
      createdAt: createdAt,
      email: decoder.decodeObjectForKey("email") as? String,
      facebookId: facebookId,
      gender: decoder.decodeObjectForKey("gender") as? String,
      name: decoder.decodeObjectForKey("name") as? String,
      picturesCount: decoder.decodeIntegerForKey("picturesCount"),
      swearings: decoder.decodeIntegerForKey("swearings"),
      avatarURL: decoder.decodeObjectForKey("avatarURL") as? String,
      superuser: decoder.decodeBoolForKey("superuser")
    )
  }
  
  public func encodeWithCoder(coder: NSCoder) {
    coder.encodeInteger(id, forKey: "id")
    coder.encodeBool(admin, forKey: "admin")
    coder.encodeObject(authToken, forKey: "authToken")
    coder.encodeObject(createdAt, forKey: "createdAt")
    coder.encodeObject(email, forKey: "email")
    coder.encodeObject(facebookId, forKey: "facebookId")
    coder.encodeObject(gender, forKey: "gender")
    coder.encodeObject(name, forKey: "name")
    coder.encodeInteger(picturesCount, forKey: "picturesCount")
    coder.encodeInteger(swearings, forKey: "swearings")
    coder.encodeObject(avatarURL, forKey: "avatarURL")
    coder.encodeBool(superuser, forKey: "superuser")
  }
  
  func fullDescription() -> String {
    return "id: \(id), admin: \(admin), authToken: \(authToken), createdAt: \(createdAt), email: \(email), facebookId: \(facebookId), gender: \(gender), name: \(name), picturesCount: \(picturesCount), swearings: \(swearings), avatarURL: \(avatarURL), superuser: \(superuser)"
  }
  
  func persist() {
    let data = NSKeyedArchiver.archivedDataWithRootObject(self)
    NSUserDefaults.standardUserDefaults().setObject(data, forKey: "&*!@(BTG*YUJhn&*(T@!&N")
    NSUserDefaults.standardUserDefaults().synchronize()
  }
  
  static func persistedUser() -> User? {
    guard let data = NSUserDefaults.standardUserDefaults().objectForKey("&*!@(BTG*YUJhn&*(T@!&N") as? NSData,
      user = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? User else {
        
        return nil
    }
    
    return user
  }
  
  static func removeUser() {
    if let _ = persistedUser() {
      NSUserDefaults.standardUserDefaults().removeObjectForKey("&*!@(BTG*YUJhn&*(T@!&N")
      NSUserDefaults.standardUserDefaults().synchronize()
    }
  }
  
  class func isLoggedInUserId(id: Int) -> Bool {
    guard let user = persistedUser() where user.id == id else {
      
      return false
    }
    
    return true
  }
}
