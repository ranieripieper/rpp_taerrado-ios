//
//  DaysAgo.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/18/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

extension NSDate {
  func daysAgo() -> String {
    let seconds = timeIntervalSinceNow
    let minutes = seconds / 60
    let hours = minutes / 60
    let days = hours / 24
    return String(Int(abs(days)))
  }
}