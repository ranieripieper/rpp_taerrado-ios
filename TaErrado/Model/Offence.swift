//
//  Offence.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/16/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation
import MapKit
import Alamofire
import Crashlytics

final class Offence: NSObject, ResponseObjectSerializable, ResponseCollectionSerializable {
  let id: Int
  let date: NSDate
  let imageURL: String
  var offendedId: Int
  var offendedImageURL: String
  var sweared: Bool
  var swearings: Int
  var comment: String
  
  var image: UIImage?
  let location: CLLocation
  var street: String?
  var city: String?
  var state: String?
  var reported = false
  
  var address: String {
    if let street = self.street where street.characters.count > 0 {
      if let city = self.city where city.characters.count > 0 {
        if let state = self.state where state.characters.count > 0 {
          return street + ", " + city + " - " + state
        }
        return street + ", " + city
      }
      return street
    } else if let city = self.city where city.characters.count > 0 {
      if let state = self.state where state.characters.count > 0 {
        return city + " - " + state
      }
      return city
    } else if let state = self.state where state.characters.count > 0 {
      return state
    } else {
      return ""
    }
  }
  
  init(image: UIImage, location: CLLocation) {
    id = 0
    date = NSDate()
    imageURL = ""
    offendedId = 0
    offendedImageURL = ""
    sweared = false
    swearings = 0
    comment = ""
    
    self.image = image
    self.location = location
  }
  
  init(_ API: [String: AnyObject], dateFormatter: NSDateFormatter?) {
    let df: NSDateFormatter
    if dateFormatter == nil {
      df = NSDateFormatter()
      df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"
    } else {
      df = dateFormatter!
    }
    id = API["id"] as? Int ?? 0
    date = df.dateFromString(API["created_at"] as? String ?? "") ?? NSDate(timeIntervalSince1970: 0)
    imageURL = API["image_url"] as? String ?? ""
    if let offended = API["user"] as? [String: AnyObject], let offendedId = offended["id"] as? Int {
      self.offendedId = offendedId
    } else {
      offendedId = 0
    }
    if let offended = API["user"] as? [String: AnyObject] {
      if let offendedProfilePictureURL = offended["profile_image"] as? String {
        offendedImageURL = offendedProfilePictureURL
      } else if let offendedFbId = offended["fb_id"] as? String {
        offendedImageURL = "http://graph.facebook.com/\(offendedFbId)/picture?type=large"
      } else {
        offendedImageURL = ""
      }
    } else {
      offendedImageURL = ""
    }
    sweared = API["user_curse"] as? Bool ?? false
    swearings = API["curses_count"] as? Int ?? 0
    comment = API["description"] as? String ?? ""
    street = API["street"] as? String ?? ""
    city = API["city"] as? String ?? ""
    state = API["state"] as? String ?? ""
    
    image = nil
    let latitude = API["lat"] as? Float ?? 0
    let longitude = API["lng"] as? Float ?? 0
    location = CLLocation(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
  }
  
  init?(response: NSHTTPURLResponse, representation: AnyObject) {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"
    id = representation["id"] as? Int ?? 0
    date = dateFormatter.dateFromString(representation["created_at"] as? String ?? "") ?? NSDate(timeIntervalSince1970: 0)
    imageURL = representation["image_url"] as? String ?? ""
    if let offended = representation["user"] as? [String: AnyObject], let offendedId = offended["id"] as? Int {
      self.offendedId = offendedId
    } else {
      offendedId = 0
    }
    if let offended = representation["user"] as? [String: AnyObject], let offendedFbId = offended["fb_id"] as? String {
      offendedImageURL = "http://graph.facebook.com/\(offendedFbId)/picture?type=large"
    } else {
      offendedImageURL = "http://graph.facebook.com/0/picture?type=large"
    }
    sweared = representation["user_curse"] as? Bool ?? false
    swearings = representation["curses_count"] as? Int ?? 0
    comment = representation["description"] as? String ?? ""
    
    image = nil
    let latitude = representation["lat"] as? Float ?? 0
    let longitude = representation["lng"] as? Float ?? 0
    location = CLLocation(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
  }
  
  static func collection(response response: NSHTTPURLResponse, representation: AnyObject) -> [Offence] {
    var offences = [Offence]()
    
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"
    
    let offencesAPI = representation.valueForKeyPath("post") as? [[String: AnyObject]] ?? representation.valueForKeyPath("posts") as? [[String: AnyObject]] ?? [[:]]
    
    for offence in offencesAPI {
      offences.append(Offence(offence, dateFormatter: dateFormatter))
    }
    
    return offences
  }
}

extension Offence: MKAnnotation {
  var title: String? { return address }
  var coordinate: CLLocationCoordinate2D { return location.coordinate }
}

// MARK: API
extension Offence {
  class func count(completion: Result<Int, NSError> -> ()) {
    Manager.sharedInstance.request(NetworkManager.Router.Count)
      .responseAPI { response in
        switch response.result {
        case .Success(let data):
          guard let JSON = data as? [String: AnyObject], success = JSON["success"] as? Bool where success, let count = JSON["count"] as? Int else {
            completion(Result.Failure(NSError(domain: "com.doisdoissete.TaErrado", code: -1, userInfo: ["description": "unable to parse response"])))
            break
          }
          completion(Result.Success(count))

        case .Failure(let error):
          completion(Result.Failure(error))
        }
    }
  }
  
  class func near(completion: Result<[Offence], NSError> -> ()) {
    Manager.sharedInstance.request(NetworkManager.Router.Home(nil, LocationManager.latitude, LocationManager.longitude))
      .responseCollection(true) { (response: Response<[Offence], NSError>) -> Void in
        switch response.result {
        case .Success(let offences):
          completion(Result.Success(offences))
        case .Failure(let error):
          completion(Result.Failure(error))
        }
    }
  }
  
  class func coordinate(coordinate: CLLocationCoordinate2D, completion: Result<[Offence], NSError> -> ()) {
    Manager.sharedInstance.request(NetworkManager.Router.Home(nil, Float(coordinate.latitude), Float(coordinate.longitude)))
      .responseCollection(true) { (response: Response<[Offence], NSError>) -> Void in
        switch response.result {
        case .Success(let offences):
          completion(Result.Success(offences))
        case .Failure(let error):
          completion(Result.Failure(error))
        }
    }
  }
  
  class func feed(page: Int, completion: Result<[Offence], NSError> -> ()) {
    Manager.sharedInstance.request(NetworkManager.Router.Home(page, nil, nil))
      .responseCollection(true) { (response: Response<[Offence], NSError>) -> Void in
        switch response.result {
        case .Success(let offences):
          completion(Result.Success(offences))
        case .Failure(let error):
          completion(Result.Failure(error))
        }
    }
  }
  
  class func byUser(id: Int?, page: Int, completion: Result<([Offence], User?), NSError> -> ()) {
    Manager.sharedInstance.request(NetworkManager.Router.User(id, page))
      .responseCollectionUser { (response: Response<[Offence], NSError>, user: User?) -> Void in
        switch response.result {
        case .Success(let offences):
          let tuple = (offences, user)
          completion(Result.Success(tuple))
        case .Failure(let error):
          completion(Result.Failure(error))
        }
    }
  }
  
  func swear(on: Bool, completion: Result<Int, NSError> -> ()) {
    Manager.sharedInstance.request(NetworkManager.Router.Swear(id, on))
      .responseAPI { response in
        DDSAnalytics.trackEvent("Swear", category: nil, label: String(self.id), value: nil, customAttributes: ["postId": String(self.id), "on": on])
        switch response.result {
        case .Success(let data):
          guard let JSON = data as? [String: AnyObject], success = JSON["success"] as? Bool where success, let count = JSON["post_curses_count"] as? Int else {
            completion(Result.Failure(NSError(domain: "com.doisdoissete.TaErrado", code: -1, userInfo: ["description": "unknown error"])))
            break
          }
          completion(Result.Success(count))
        case .Failure(let error):
          completion(Result.Failure(error))
        }
    }
  }
  
  func report(completion: Result<Bool, NSError> -> ()) {
    Manager.sharedInstance.request(NetworkManager.Router.Report(id))
      .responseAPI { response in
        switch response.result {
        case .Success(let data):
          guard let JSON = data as? [String: AnyObject], success = JSON["success"] as? Bool where success else {
            completion(Result.Failure(NSError(domain: "com.doisdoissete.TaErrado", code: -1, userInfo: ["description": "unknown error"])))
            break
          }
          completion(Result.Success(true))
        case .Failure(let error):
          completion(Result.Failure(error))
        }
    }
  }
  
  func upload(completion: Result<Offence, NSError> -> ()) {
    getAddress() { result in
      switch result {
      case .Failure(let error):
        completion(Result.Failure(error))
      case .Success:
        let latitude = String(format: "%.6f", arguments: [self.location.coordinate.latitude])
        let longitude = String(format: "%.6f", arguments: [self.location.coordinate.longitude])
        
        guard let newImageData = UIImagePNGRepresentation(self.image!), authToken = NetworkManager.Router.authToken?.dataUsingEncoding(NSUTF8StringEncoding), latData = latitude.dataUsingEncoding(NSUTF8StringEncoding), longData = longitude.dataUsingEncoding(NSUTF8StringEncoding), descData = self.comment.dataUsingEncoding(NSUTF8StringEncoding), streetData = (self.street ?? "").dataUsingEncoding(NSUTF8StringEncoding), cityData = (self.city ?? "").dataUsingEncoding(NSUTF8StringEncoding), stateData = (self.state ?? "").dataUsingEncoding(NSUTF8StringEncoding) else {
          
          return
        }

        let URLRequest: URLRequestConvertible = NetworkManager.Router.Upload
        Manager.sharedInstance.upload(URLRequest, multipartFormData: { (multipartFormData: MultipartFormData) in
          multipartFormData.appendBodyPart(data: newImageData, name: "post[image]", fileName: "\(self.offendedId).png", mimeType: "image/png")
          multipartFormData.appendBodyPart(data: authToken, name: "auth_token")
          multipartFormData.appendBodyPart(data: descData, name: "post[description]")
          multipartFormData.appendBodyPart(data: latData, name: "post[lat]")
          multipartFormData.appendBodyPart(data: longData, name: "post[lng]")
          multipartFormData.appendBodyPart(data: streetData, name: "post[street]")
          multipartFormData.appendBodyPart(data: cityData, name: "post[city]")
          multipartFormData.appendBodyPart(data: stateData, name: "post[state]")
          }) { encodingResult in
            switch encodingResult {
            case .Success(let upload, _, _):
              upload.responseObject { (response: Response<Offence, NSError>) in
                switch response.result {
                case .Success(let offence):
                  DDSAnalytics.trackEvent("Upload", category: "UX", label: offence.comment, value: nil, customAttributes: nil)
                  NSNotificationCenter.defaultCenter().postNotificationName(PostOffenceNotification, object: nil, userInfo: ["offence": offence])
                  completion(Result.Success(offence))
                case .Failure(let error):
                  completion(Result.Failure(error))
                }
              }
            case .Failure:
              completion(Result.Failure(NSError(domain: "com.doisdoissete.TaErrado", code: -1, userInfo: ["description": "unknown error"])))
            }
        }
      }
    }
  }
  
  func getAddress(completion: Result<Offence, NSError> -> ()) {
    Manager.sharedInstance.request(GooglePlacesAPI.Router.Zipcode(Float(location.coordinate.latitude), Float(location.coordinate.longitude)))
      .responseCollection(false) { (response: Response<[Address], NSError>) in
        switch response.result {
        case .Success(let addresses):
          self.street = addresses.first?.street
          self.city = addresses.first?.city
          self.state = addresses.first?.state
          completion(Result.Success(self))
        case .Failure(let error):
          completion(Result.Failure(error))
        }
    }
  }
}
