//
//  OffenceAnnotation.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/16/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class OffenceAnnotation: NSObject {
  var offence: Offence
  
  init(offence: Offence) {
    self.offence = offence
  }
}

extension OffenceAnnotation: MKAnnotation {
  var title: String? { return offence.address }
  var coordinate: CLLocationCoordinate2D { return offence.location.coordinate }
}
