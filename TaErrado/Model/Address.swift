//
//  Address.swift
//  TaErrado
//
//  Created by Gilson Gil on 9/28/15.
//  Copyright © 2015 doisdoissete. All rights reserved.
//

import Foundation
import Alamofire

public struct Address: ResponseObjectSerializable, ResponseCollectionSerializable {
  let street: String
  let city: String
  let state: String
  
  public init?(response: NSHTTPURLResponse, representation: AnyObject) {
    guard let dict = representation as? [String: AnyObject], addressComponents = dict["address_components"] as? [[String: AnyObject]] where addressComponents.count > 0 else {
      return nil
    }
    
    street = addressComponents.filter {
      if let types = $0["types"] as? [String] {
        return types.indexOf("route") != nil
      }
      return false
    }.first?["long_name"] as? String ?? ""
    
    city = addressComponents.filter {
      if let types = $0["types"] as? [String] {
        return types.indexOf("locality") != nil
      }
      return false
    }.first?["long_name"] as? String ?? ""
    
    state = addressComponents.filter {
      if let types = $0["types"] as? [String] {
        return types.indexOf("administrative_area_level_1") != nil
      }
      return false
    }.first?["short_name"] as? String ?? ""
  }
  
  public static func collection(response response: NSHTTPURLResponse, representation: AnyObject) -> [Address] {
    var addresses = [Address]()
    
    let addressesAPI = representation.valueForKeyPath("results") as? [[String: AnyObject]] ?? [[:]]
    
    for addressAPI in addressesAPI {
      guard let address = Address(response: response, representation: addressAPI) else {
        continue
      }
      addresses.append(address)
    }
    
    return addresses
  }
}
