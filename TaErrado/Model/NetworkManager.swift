//
//  NetworkManager.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation
import Alamofire

// MARK: - Image

extension Request {
  /**
  Creates a response serializer that returns a UIImage object constructed from the response data using
  `UIImage(data:scale)`.
  
  - returns: A UIImage object response serializer.
  */
  public static func ImageResponseSerializer() -> ResponseSerializer<UIImage, NSError> {
    return ResponseSerializer { _, _, data, error in
      guard error == nil else {
        
        return .Failure(error!)
      }
      
      guard let validData = data where validData.length > 0, let image = UIImage(data: validData, scale: UIScreen.mainScreen().scale) else {
        let failureReason = "UIImage could not be serialized. Input data was nil or zero length."
        let error = Error.errorWithCode(.JSONSerializationFailed, failureReason: failureReason)
        return .Failure(error)
      }
      
      return .Success(image)
    }
  }
  
  /**
  Adds a handler to be called once the request has finished.
  
  - parameter completionHandler: A closure to be executed once the request has finished.
  
  - returns: The request.
  */
  public func responseImage(completionHandler: Response<UIImage, NSError> -> Void) -> Self {
    
    return response(responseSerializer: Request.ImageResponseSerializer(), completionHandler: completionHandler)
  }
}

// MARK: - Generic Response Serializer

public protocol ResponseObjectSerializable {
  init?(response: NSHTTPURLResponse, representation: AnyObject)
}

extension Request {
  public func responseObject<T: ResponseObjectSerializable>(completionHandler: Response<T, NSError> -> Void) -> Self {
    let responseSerializer = ResponseSerializer<T, NSError> { request, response, data, error in
      guard error == nil else { return .Failure(error!) }
      
      let JSONResponseSerializer = Request.APIResponseSerializer()
      let result = JSONResponseSerializer.serializeResponse(request, response, data, error)
      
      switch result {
      case .Success(let value):
        if let
          response = response,
          responseObject = T(response: response, representation: value)
        {
          return .Success(responseObject)
        } else {
          let failureReason = "JSON could not be serialized into response object: \(value)"
          let error = Error.errorWithCode(.JSONSerializationFailed, failureReason: failureReason)
          return .Failure(error)
        }
      case .Failure(let error):
        return .Failure(error)
      }
    }
    
    return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
  }
}

public protocol ResponseCollectionSerializable {
  static func collection(response response: NSHTTPURLResponse, representation: AnyObject) -> [Self]
}

extension Request {
  public func responseCollection<T: ResponseCollectionSerializable>(API: Bool, completionHandler: Response<[T], NSError> -> Void) -> Self {
    let responseSerializer = ResponseSerializer<[T], NSError> { request, response, data, error in
      guard error == nil else { return .Failure(error!) }
      
      let JSONSerializer: ResponseSerializer<AnyObject, NSError>
      if API {
        JSONSerializer = Request.APIResponseSerializer()
      } else {
        JSONSerializer = Request.JSONResponseSerializer()
      }
      let result = JSONSerializer.serializeResponse(request, response, data, error)
      
      switch result {
      case .Success(let value):
        if let response = response {
          return .Success(T.collection(response: response, representation: value))
        } else {
          let failureReason = "Response collection could not be serialized due to nil response"
          let error = Error.errorWithCode(.JSONSerializationFailed, failureReason: failureReason)
          return .Failure(error)
        }
      case .Failure(let error):
        return .Failure(error)
      }
    }
    
    return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
  }
  
  public func responseCollectionUser<T: ResponseCollectionSerializable>(completionHandler: (Response<[T], NSError>, User?) -> Void) -> Self {
    var user: User?
    let responseSerializer = ResponseSerializer<[T], NSError> { request, response, data, error in
      guard error == nil else {
        
        return .Failure(error!)
      }
      
      let JSONSerializer = Request.APIResponseSerializer()
      let result = JSONSerializer.serializeResponse(request, response, data, error)
      
      if let userAPI = result.value?.valueForKeyPath("user") as? [String: AnyObject] {
        user = User(userAPI, dateFormatter: nil)
      }
      
      switch result {
      case .Success(let value):
        if let response = response {
          return .Success(T.collection(response: response, representation: value))
        } else {
          let failureReason = "Response collection could not be serialized due to nil response"
          let error = Alamofire.Error.errorWithCode(.JSONSerializationFailed, failureReason: failureReason)
          return .Failure(error)
        }
      case .Failure(let error):
        return .Failure(error)
      }
    }
    
    return response(responseSerializer: responseSerializer) { response in
      completionHandler(response, user)
    }
  }
}

// MARK: - JSON with error

extension Request {
  
  /**
  Creates a response serializer that returns a JSON object constructed from the response data using
  `NSJSONSerialization` with the specified reading options.
  
  - parameter options: The JSON serialization reading options. `.AllowFragments` by default.
  
  - returns: A JSON object response serializer.
  */
  public static func APIResponseSerializer() -> ResponseSerializer<AnyObject, NSError> {
    
    return ResponseSerializer { _, _, data, error in
      guard error == nil else { return .Failure(error!) }
      
      guard let validData = data where validData.length > 0 else {
        let failureReason = "JSON could not be serialized. Input data was nil or zero length."
        let error = Error.errorWithCode(.JSONSerializationFailed, failureReason: failureReason)
        return .Failure(error)
      }
      
      do {
        let JSON = try NSJSONSerialization.JSONObjectWithData(validData, options: .AllowFragments)
        if let success = JSON["success"] as? Bool where success {
          return .Success(JSON)
        } else {
          let failureReason: String
          if let error = JSON["error"] as? String {
            failureReason = error
          } else if let errors = JSON["errors"] as? [String] {
            failureReason = errors.reduce("") {
              $0 + "\n" + $1
            }
          } else {
            failureReason = "Erro desconhecido."
          }
          let error = NSError(domain: "com.doisdoissete.TaErrado", code: JSON["code"] as? Int ?? -1, userInfo: [NSLocalizedDescriptionKey: failureReason])
          return .Failure(error)
        }
      } catch {
        return .Failure(error as NSError)
      }
    }
  }
  
  /**
  Adds a handler to be called once the request has finished.
  
  - parameter options:           The JSON serialization reading options. `.AllowFragments` by default.
  - parameter completionHandler: A closure to be executed once the request has finished.
  
  - returns: The request.
  */
  public func responseAPI(completionHandler: Response<AnyObject, NSError> -> Void) -> Self {

    return response(responseSerializer: Request.APIResponseSerializer(), completionHandler: completionHandler)
  }
}

struct NetworkManager {
  enum Router: URLRequestConvertible {
    static let baseURLString = "http://www.taerradoapp.co/api/v1"
//    static let baseURLString = "http://doisdoissete.cloudapp.net/api/v1"
//    static let baseURLString = "https://ta-errado-stg.herokuapp.com/api/v1"
    static var authToken: String?
    
    case Count
    case Home(Int?, Float?, Float?)
    case User(Int?, Int)
    case Swear(Int, Bool)
    case Report(Int)
    case CreateUser([String: AnyObject]?)
    case Login(String, String)
    case ForgotPassword(String)
    case NewPassword(String, String)
    case EditProfile([String: AnyObject]?)
    case ChangePassword(String)
    case DeleteAccount
    case Upload
    case Parse
    
    var method: Alamofire.Method {
      switch self {
      case .CreateUser, .Login, .Swear, .Report, Upload, .ForgotPassword, .Parse:
        return .POST
      case .NewPassword, .EditProfile, .ChangePassword:
        return .PUT
      case .DeleteAccount:
        return .DELETE
      default:
        return .GET
      }
    }
    
    var path: String {
      switch self {
      case .Count:
        return "/posts/count"
      case .Home:
        return "/posts"
      case .User(let id, _):
        if let id = id {
          return "/users/\(id)/posts"
        } else {
          return "/users/posts"
        }
      case .Swear(let id, let on):
        if on {
          return "/posts/\(id)/curse"
        } else {
          return "/posts/\(id)/uncurse"
        }
      case .Report(let id):
        return "/posts/\(id)/report"
      case .CreateUser:
        return "/users"
      case .Login:
        return "/users/auth"
      case .ForgotPassword:
        return "/users/password_reset"
      case .NewPassword(let token, _):
        return "/users/password_reset/\(token)"
      case .EditProfile:
        return "/users"
      case .DeleteAccount:
        return "/users"
      case .ChangePassword:
        return "/users"
      case .Upload:
        return "/posts"
      default:
        return ""
      }
    }
    
    var URLRequest: NSMutableURLRequest {
      let URL = NSURL(string: Router.baseURLString)!
      let mutableURLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(path))
      mutableURLRequest.HTTPMethod = method.rawValue
      
      switch self {
      case .Count:
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let params = ["date": dateFormatter.stringFromDate(NSDate())]
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: params).0
      case .Home(let page, let latitude, let longitude):
        var params = [String: AnyObject]()
        if let page = page {
          params["page"] = page
          if let authToken = Router.authToken {
            params["auth_token"] = authToken
          }
        } else {
          params["lat"] = latitude!
          params["lng"] = longitude!
          if let authToken = Router.authToken {
            params["auth_token"] = authToken
          }
        }
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: params).0
      case .User(_, let page):
        var params: [String: AnyObject] = ["page": page]
        if let authToken = Router.authToken {
          params["auth_token"] = authToken
        }
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: params).0
      case .Swear, .Report:
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: ["auth_token": Router.authToken!]).0
      case .CreateUser(let parameters):
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
      case .Login(let email, let password):
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: ["user": ["email": email, "password": password]]).0
      case .ForgotPassword(let email):
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: ["user": ["email": email]]).0
      case .NewPassword(_, let password):
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: ["user": ["password": password, "password_confirmation": password]]).0
      case .EditProfile(let parameters):
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
      case .ChangePassword(let password):
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: ["user": ["password": password, "password_confirmation": password]]).0
      case .DeleteAccount:
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0
      case .Upload:
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0
      default:
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0
      }
    }
  }
}
