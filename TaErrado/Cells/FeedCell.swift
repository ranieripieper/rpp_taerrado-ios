//
//  FeedCell.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Alamofire
import PINRemoteImage

class FeedCell: UITableViewCell {
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var offendedButton: UIButton!
  @IBOutlet weak var offenceImageView: UIImageView!
  @IBOutlet weak var swearButton: UIButton!
  @IBOutlet weak var swearingsLabel: UILabel!
  @IBOutlet weak var optionsButton: UIButton!
  @IBOutlet weak var commentLabel: UILabel!
  
  var offence: Offence?
  var reportCallback: (FeedCell -> ())?
  var goToUserCallback: (FeedCell -> ())?
  var swearCallback: ((FeedCell, Bool) -> ())?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    commentLabel.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 24 * 2
    offendedButton.layer.borderColor = UIColor(red: 253 / 255, green: 211 / 255, blue: 17 / 255, alpha: 1).CGColor
    let tap = UITapGestureRecognizer(target: self, action: #selector(FeedCell.swearTapped(_:)))
    tap.numberOfTapsRequired = 2
    addGestureRecognizer(tap)
  }
  
  func configureWithOffence(offence: Offence, dateFormatter: NSDateFormatter, fromProfile: Bool) {
    self.offence = offence
    addressLabel.text = offence.address
    dateLabel.text = dateFormatter.stringFromDate(offence.date)
    offendedButton.setImage(UIImage(named: "img_placeholder"), forState: .Normal)
    offenceImageView.image = nil
    offenceImageView.alpha = 0
    swearButton.selected = offence.sweared
    if offence.swearings == 1 {
      swearingsLabel.text = "\(offence.swearings) xingamento"
    } else {
      swearingsLabel.text = "\(offence.swearings) xingamentos"
    }
    commentLabel.text = offence.comment
    optionsButton.hidden = offence.reported
    
    if let url = NSURL(string: offence.imageURL) {
      offenceImageView.pin_setImageFromURL(url) { result in
        if let user = User.persistedUser() where user.superuser {
          self.offenceImageView.image = result.image
        } else {
          self.offenceImageView.image = result.image.feedBlurredImage()
        }
        UIView.animateWithDuration(0.3) {
          self.offenceImageView.alpha = 1
        }
      }
    }
    
    if let url = NSURL(string: offence.offendedImageURL) {
      offendedButton.pin_setImageFromURL(url, placeholderImage: UIImage(named: "img_placeholder"))
    }
  }
  
  func updateSwearings(offence: Offence) {
    if offence == offence {
      if offence.swearings == 1 {
        swearingsLabel.text = "\(offence.swearings) xingamento"
      } else {
        swearingsLabel.text = "\(offence.swearings) xingamentos"
      }
    }
  }
  
  func updateReported(offence: Offence) {
    if offence == self.offence {
      optionsButton.hidden = offence.reported
    }
  }
  
  // MARK: Actions
  @IBAction func swearTapped(sender: UIButton) {
    swearCallback?(self, !swearButton.selected)
  }
  
  @IBAction func optionsTapped(sender: UIButton) {
    reportCallback?(self)
  }
  
  @IBAction func goToOffended(sender: UIButton) {
    if let user = User.persistedUser() where user.superuser && user.admin {
      goToUserCallback?(self)
    }
  }
}
