//
//  ImagePicker.swift
//
//  Created by Gilson Gil on 3/19/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import AssetsLibrary
import MapKit
import Async

final class ImagePicker: NSObject {
  private var viewController: UIViewController
  private var callback: (UIImage?, CLLocation?) -> ()
  
  class func presentInViewController(viewController: UIViewController, callback: (UIImage?, CLLocation?) -> ()) -> ImagePicker {
    let imagePicker = ImagePicker(viewController: viewController, callback: callback)
    imagePicker.presentImagePicker()
    return imagePicker
  }
  
  class func presentOptionsInViewController(viewController: UIViewController, callback: UIImage? -> ()) -> ImagePicker {
    let imagePicker = ImagePicker(viewController: viewController, callback: callback)
    imagePicker.presentOptionsImagePicker()
    return imagePicker
  }
  
  private init(viewController: UIViewController, callback: (UIImage?, CLLocation?) -> ()) {
    self.viewController = viewController
    self.callback = callback
  }
  
  private init(viewController: UIViewController, callback: UIImage? -> ()) {
    self.viewController = viewController
    self.callback = { image, location in
      callback(image)
    }
  }
  
  private func presentImagePicker() {
    let imagePicker = UIImagePickerController()
    imagePicker.delegate = self
    imagePicker.sourceType = .PhotoLibrary
    Async.main {
      self.viewController.presentViewController(imagePicker, animated: true, completion: nil)
    }
  }
  
  private func presentOptionsImagePicker() {
    let imagePicker = UIImagePickerController()
    imagePicker.delegate = self
    if UIImagePickerController.isCameraDeviceAvailable(.Rear) || UIImagePickerController.isCameraDeviceAvailable(.Front) {
      let alertController = UIAlertController(title: "Escolha", message: nil, preferredStyle: .ActionSheet)
      alertController.addAction(UIAlertAction(title: "Camera", style: .Default) { _ in
        Async.main {
          imagePicker.sourceType = .Camera
          imagePicker.allowsEditing = true
          self.viewController.presentViewController(imagePicker, animated: true, completion: nil)
        }
        })
      alertController.addAction(UIAlertAction(title: "Biblioteca", style: .Default) { _ in
        Async.main {
          imagePicker.sourceType = .PhotoLibrary
          imagePicker.allowsEditing = true
          self.viewController.presentViewController(imagePicker, animated: true, completion: nil)
        }
        })
      alertController.addAction(UIAlertAction(title: "Cancelar", style: .Cancel) { _ in
        self.callback(nil, nil)
        })
      self.viewController.presentViewController(alertController, animated: true, completion: nil)
    } else {
      Async.main {
        imagePicker.sourceType = .PhotoLibrary
        imagePicker.allowsEditing = true
        self.viewController.presentViewController(imagePicker, animated: true, completion: nil)
      }
    }
  }
}

extension ImagePicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
    let image: UIImage?
    if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
      image = editedImage
    } else {
      image = info[UIImagePickerControllerOriginalImage] as? UIImage
    }
    if let image = image {
      if let url = info[UIImagePickerControllerReferenceURL] as? NSURL {
        let library = ALAssetsLibrary()
        library.assetForURL(url, resultBlock: { asset in
          let location = asset?.valueForProperty(ALAssetPropertyLocation) as? CLLocation
          self.callback(image, location)
          Async.main {
            self.viewController.dismissViewControllerAnimated(true, completion: nil)
          }
          }, failureBlock: { error in
            self.callback(image, nil)
            Async.main {
              self.viewController.dismissViewControllerAnimated(true, completion: nil)
            }
        })
      } else {
        self.callback(image, nil)
        Async.main {
          self.viewController.dismissViewControllerAnimated(true, completion: nil)
        }
      }
    } else {
      self.callback(nil, nil)
      Async.main {
        self.viewController.dismissViewControllerAnimated(true, completion: nil)
      }
    }
  }
  
  func imagePickerControllerDidCancel(picker: UIImagePickerController) {
    callback(nil, nil)
    Async.main {
      self.viewController.dismissViewControllerAnimated(true, completion: nil)
    }
  }
}