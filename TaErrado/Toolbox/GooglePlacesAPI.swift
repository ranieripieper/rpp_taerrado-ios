//
//  GooglePlacesAPI.swift
//
//  Created by Gilson Gil on 3/5/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Alamofire

struct GooglePlacesAPI {
  enum Router: URLRequestConvertible {
    static let types = "geocode"
    static let location = "-15.733494,-47.939065"
    static let radius = "50000"
    static let language = "pt"
    static let apiKey = "AIzaSyB-6sPlmCll_zOBy4b3UrbZ9kwZnQgJNfo"
    static let baseURLString = "https://maps.googleapis.com/maps/api/geocode/"
    
    case Zipcode(Float, Float)
    
    var method: Alamofire.Method {
      return .GET
    }
    
    var path: String {
      return "json"
    }
    
    var URLRequest: NSMutableURLRequest {
      let URL = NSURL(string: Router.baseURLString)!
      let mutableURLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(path))
      mutableURLRequest.HTTPMethod = method.rawValue
      
      switch self {
      case .Zipcode(let latitude, let longitude):
        let params = ["latlng": String(format: "%.6f,%.6f", arguments: [latitude, longitude]), "key": Router.apiKey]
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: params).0
      }
    }
  }
}
