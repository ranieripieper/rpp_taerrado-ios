//
//  CaptureView.swift
//
//  Created by Gilson Gil on 3/12/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import AVFoundation
import ImageIO

class CaptureView: UIView {
  let captureSession: AVCaptureSession
  var stillImageOutput: AVCaptureStillImageOutput?
  weak var videoPreviewLayer: AVCaptureVideoPreviewLayer?
  var stillImage: UIImage?
  
  required init?(coder aDecoder: NSCoder) {
    captureSession = AVCaptureSession()
    captureSession.sessionPreset = AVCaptureSessionPresetPhoto
    super.init(coder: aDecoder)
  }
  
  // MARK: Capture Session Configuration
  func addVideoPreviewLayer() {
    videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
    videoPreviewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
    videoPreviewLayer!.frame = bounds
    layer.addSublayer(videoPreviewLayer!)
  }
  
  func removeVideoPreviewLayer() {
    videoPreviewLayer?.removeFromSuperlayer()
    videoPreviewLayer = nil
  }
  
  func addVideoInput() {
    if let videoDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo) {
      do {
        let videoIn = try AVCaptureDeviceInput(device: videoDevice)
        if captureSession.canAddInput(videoIn) {
          captureSession.addInput(videoIn)
        }
      } catch {
        
      }
    }
  }
  
  func removeVideoInput() {
    if let input = captureSession.inputs.first as? AVCaptureDeviceInput {
      captureSession.removeInput(input)
    }
  }
  
  func addStillImageOutput() {
    stillImageOutput = AVCaptureStillImageOutput()
    stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
    captureSession.addOutput(stillImageOutput)
  }
  
  func removeStillImageOutput() {
    captureSession.removeOutput(stillImageOutput)
    stillImageOutput = nil
  }
  
  // MARK: Methods
  func captureStillImage(completion: UIImage? -> ()) {
    var videoConnection: AVCaptureConnection?
    for connection in stillImageOutput!.connections {
      for port in connection.inputPorts! {
        if port.mediaType == AVMediaTypeVideo {
          videoConnection = connection as? AVCaptureConnection
          break
        }
      }
      if videoConnection != nil {
        break
      }
    }
    stillImageOutput?.captureStillImageAsynchronouslyFromConnection(videoConnection) { imageDataSampleBuffer, error in
      if CMGetAttachment(imageDataSampleBuffer, kCGImagePropertyExifDictionary, nil) == nil {
        return
      }
      let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
      let image = UIImage(data: imageData)
      let cropFrame = CGRect(x: (image!.size.height - image!.size.width) / 2, y: 0, width: image!.size.width, height: image!.size.width)
      if let imageRef = CGImageCreateWithImageInRect(image!.CGImage, cropFrame) {
        let finalImage = UIImage(CGImage: imageRef, scale: image!.scale, orientation: self.getMirroredOrientation(image!.imageOrientation))
        
        let newSize = CGSize(width: 600, height: 600)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1)
        finalImage.drawInRect(CGRect(origin: CGPointZero, size: newSize))
        self.stillImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
      } else {
        self.stillImage = nil
      }
      
      self.captureSession.stopRunning()
      self.removeVideoInput()
      self.removeStillImageOutput()
      self.removeVideoPreviewLayer()
      completion(self.stillImage)
    }
  }

  func getMirroredOrientation(orientation: UIImageOrientation) -> UIImageOrientation {
    if let input = captureSession.inputs.last as? AVCaptureDeviceInput {
      if input.device.position == .Back {
        return orientation
      }
    }
    switch orientation {
    case .Up:
      return .UpMirrored
    case .Left:
      return .RightMirrored
    case .Down:
      return .DownMirrored
    case .Right:
      return .LeftMirrored
    default:
      return orientation
    }
  }
  
  func canChangeVideoInput() -> Bool {
    let videoDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
    return videoDevices.count > 1
  }
  
  func changeVideoInput() {
    if !canChangeVideoInput() { return }
    
    let videoDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo).filter { device in
      device.position != self.captureSession.inputs.last!.device!.position
    }
    captureSession.removeInput(captureSession.inputs.last as! AVCaptureDeviceInput)
    
    if let videoDevice = videoDevices.last as? AVCaptureDevice {
      do {
        let videoIn = try AVCaptureDeviceInput(device: videoDevice)
        if captureSession.canAddInput(videoIn) {
          captureSession.addInput(videoIn)
        }
      } catch {
        
      }
    }
  }
}
