//
//  ImageHandler.swift
//  TaErrado
//
//  Created by Gilson Gil on 9/4/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

extension UIImage {
  func feedBlurredImage() -> UIImage? {
    return applyBlurWithRadius(2, tintColor: UIColor(white: 0, alpha: 0.2), saturationDeltaFactor: 1, maskImage: nil)
  }
  
  func postBlurredImage() -> UIImage? {
    return applyBlurWithRadius(3, tintColor: UIColor.clearColor(), saturationDeltaFactor: 1, maskImage: nil)
  }
  
  func profileBlurredImage() -> UIImage? {
    return applyBlurWithRadius(5, tintColor: UIColor(red: 67 / 255, green: 66 / 255, blue: 66 / 255, alpha: 0.4), saturationDeltaFactor: 1, maskImage: nil)
  }
}
