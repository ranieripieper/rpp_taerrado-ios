//
//  LocationManager.swift
//
//  Created by Gilson Gil on 10/30/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

protocol LocationManagerDelegate {
  func didUpdateLocation(userLocation: CLLocation)
  func didRespondToRequestWithStatus(status: CLAuthorizationStatus)
}

class LocationManager: CLLocationManager {
  var locationManagerDelegate: LocationManagerDelegate?
  var userLocation: CLLocation!
  class var latitude: Float {
    if let location = LocationManager.sharedInstance.location {
      return Float(location.coordinate.latitude)
    } else {
      return 0
    }
  }
  class var longitude: Float {
    if let location = LocationManager.sharedInstance.location {
      return Float(location.coordinate.longitude)
    } else {
      return 0
    }
  }
  
  class var sharedInstance: LocationManager {
    struct Static {
      static var instance: LocationManager?
      static var token: dispatch_once_t = 0
    }
    dispatch_once(&Static.token) {
      Static.instance = LocationManager()
    }
    return Static.instance!
  }
  
  override init() {
    super.init()
    delegate = self
    desiredAccuracy = kCLLocationAccuracyBest
    requestWhenInUseAuthorization()
  }
  
  class func isAuthorized() -> Bool {
    return authorizationStatus() == .AuthorizedWhenInUse
  }
}

extension LocationManager: CLLocationManagerDelegate {
  func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
    if status == .AuthorizedWhenInUse {
      startUpdatingLocation()
    }
  }
  
  func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    if let location = locations.last {
      if userLocation == nil || location.distanceFromLocation(userLocation) > 100 {
        userLocation = location
        locationManagerDelegate?.didUpdateLocation(userLocation)
      }
    }
  }
  
  func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
    
  }
}
