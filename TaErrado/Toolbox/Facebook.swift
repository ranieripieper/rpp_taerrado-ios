//
//  Facebook.swift
//
//  Created by Gilson Gil on 4/2/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import FBSDKShareKit
import Alamofire

let FacebookCancelledErrorCode = 1
let FacebookCancelledErrorDomain = "TaErradoFacebookError"

struct Facebook {
  func loginWithFacebookFromViewController(viewController: UIViewController, closure: (Result<[String: AnyObject], NSError> -> ())?) {
    let login = FBSDKLoginManager()
    login.loginBehavior = .Native
    login.logInWithReadPermissions(["public_profile"], fromViewController: viewController) { result, error in
      if let error = error {
        closure?(Result.Failure(error))
      } else if result.isCancelled {
        closure?(Result.Failure(NSError(domain: FacebookCancelledErrorDomain, code: FacebookCancelledErrorCode, userInfo: ["description": "user cancelled"])))
      } else {
        self.persistToken(result.token)
        self.getUserData(closure)
      }
    }
  }
  
  func getUserData(completion: (Result<[String: AnyObject], NSError> -> ())?) {
    if let currentAccessToken = FBSDKAccessToken.currentAccessToken().tokenString {
      FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "first_name,last_name,gender,picture.type(large){url}"]).startWithCompletionHandler() { _, result, error in
        if let error = error {
          completion?(Result.Failure(error))
        } else {
          let name = result["first_name"] as? String ?? ""
          let lastName = result["last_name"] as? String ?? ""
          let gender = result["gender"] as? String ?? ""
          let id = result["id"] as? String ?? ""
          let email = result["email"] as? String ?? "\(id)@facebook.com"
//          let avatar = result.valueForKeyPath("picture.data.url") as? String ?? ""
          if gender.characters.count > 0 {
            NSUserDefaults.standardUserDefaults().setObject(gender, forKey: "Gender")
            NSUserDefaults.standardUserDefaults().synchronize()
          }
          var user = ["name": name, "surname": lastName, "gender": gender, "fb_token": currentAccessToken, "fb_id": id, "email": email]
          if let deviceToken = PushNotification.persistedToken() {
            user["device_token"] = deviceToken
            user["platform"] = "ios"
          }
          completion?(Result.Success(["user": user]))
        }
      }
    }
  }
  
  func askPublishPermissionsFromViewController(viewController: UIViewController, completion: Result<Bool, NSError> -> ()) {
    if hasPermissions() {
      completion(Result.Success(true))
    } else {
      let login = FBSDKLoginManager()
      login.loginBehavior = .Native
      login.logInWithPublishPermissions(["publish_actions"], fromViewController: viewController) { result, error in
        if let error = error {
          completion(Result.Failure(error))
        } else if result.isCancelled {
          completion(Result.Failure(NSError(domain: "com.doisdoissete.TaErrado", code: -1, userInfo: ["description": "user cancelled"])))
        } else {
          self.persistToken(result.token)
          completion(Result.Success(true))
        }
      }
    }
  }
  
  func postImage(image: UIImage, message: String) {
    currentAccessToken()
    let photo = FBSDKSharePhoto(image: image, userGenerated: true)
    photo.caption = message
    let content = FBSDKSharePhotoContent()
    content.photos = [photo]
    FBSDKShareAPI.shareWithContent(content, delegate: nil)
  }
  
  private func persistToken(token: FBSDKAccessToken) {
    let data = NSKeyedArchiver.archivedDataWithRootObject(token)
    NSUserDefaults.standardUserDefaults().setObject(data, forKey: "HUFihsdf8239")
    NSUserDefaults.standardUserDefaults().synchronize()
  }
  
  private func currentAccessToken() -> FBSDKAccessToken? {
    if let current = FBSDKAccessToken.currentAccessToken() {
      return current
    }
    if let data = NSUserDefaults.standardUserDefaults().dataForKey("HUFihsdf8239"), let accessToken = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? FBSDKAccessToken {
      FBSDKAccessToken.setCurrentAccessToken(accessToken)
      return accessToken
    }
    return nil
  }
  
  func hasPermissions() -> Bool {
    return currentAccessToken()?.hasGranted("publish_actions") ?? false
  }
}
