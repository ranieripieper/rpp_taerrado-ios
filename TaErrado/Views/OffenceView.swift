//
//  OffenceView.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/16/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import MapKit
import Alamofire

protocol OffenceViewDelegate {
  func didRemove()
  func goToOffence(offence: Offence, fromOffenceView offenceView: OffenceView)
}

final class OffenceView: MKAnnotationView {
  private var circle: UIImageView?
  private var button: UIButton?
  private var addressLabel: UILabel?
  private var descriptionLabel: UILabel?
  private var dateLabel: UILabel?
  var delegate: OffenceViewDelegate?
  weak var offence: Offence?
  private var buttonWidthConstraint: NSLayoutConstraint?
  
  var imageRequest: Request?
  
  private let circleDiameter: CGFloat = 100
  var circleFrame: CGRect {
    return circle?.frame ?? CGRect.null
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundColor = UIColor.clearColor()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    backgroundColor = UIColor.clearColor()
  }
  
  override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
    super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
    backgroundColor = UIColor.clearColor()
    
    button = UIButton(type: .Custom)
    button?.frame = CGRect(x: 350 / 2, y: 8, width: 0, height: circleDiameter - 8 * 2)
    button?.clipsToBounds = true
    button?.backgroundColor = UIColor(red: 64 / 255, green: 64 / 255, blue: 65 / 255, alpha: 1)
    button?.hidden = true
    button?.addTarget(self, action: #selector(OffenceView.buttonTapped(_:)), forControlEvents: .TouchUpInside)
    button?.translatesAutoresizingMaskIntoConstraints = false
    addSubview(button!)
    let buttonHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-(175)-[button]", options: .AlignAllCenterY, metrics: nil, views: ["button": button!])
    buttonWidthConstraint = NSLayoutConstraint(item: button!, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 0)
    let buttonVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-(8)-[button(height)]", options: .AlignAllCenterX, metrics: ["height": circleDiameter - 8 * 2], views: ["button": button!])
    addConstraints(buttonHorizontalConstraints)
    addConstraints(buttonVerticalConstraints)
    button!.addConstraint(buttonWidthConstraint!)
    
    descriptionLabel = UILabel(frame: CGRect(x: 58, y: button!.bounds.height / 2 - 4, width: button!.bounds.width - circleDiameter / 2 - 8 * 2, height: 16))
    descriptionLabel!.textColor = UIColor.whiteColor()
    descriptionLabel!.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
    descriptionLabel!.minimumScaleFactor = 0.8
    descriptionLabel!.adjustsFontSizeToFitWidth = true
    descriptionLabel!.numberOfLines = 1
    button!.addSubview(descriptionLabel!)
    descriptionLabel!.translatesAutoresizingMaskIntoConstraints = false
    let horizontalConstraints1 = NSLayoutConstraint.constraintsWithVisualFormat("H:[description]-(8)-|", options: .AlignAllCenterY, metrics: nil, views: ["description": descriptionLabel!])
    let leftConstraint = NSLayoutConstraint(item: descriptionLabel!, attribute: .Left, relatedBy: .Equal, toItem: button!, attribute: .Left, multiplier: 1, constant: 58)
    leftConstraint.priority = 900
    let centerConstraint = NSLayoutConstraint(item: descriptionLabel!, attribute: .CenterY, relatedBy: .Equal, toItem: button!, attribute: .CenterY, multiplier: 1, constant: 0)
    button!.addConstraints(horizontalConstraints1)
    button!.addConstraint(leftConstraint)
    button!.addConstraint(centerConstraint)
    
    addressLabel = UILabel(frame: CGRect(x: 58, y: button!.bounds.height / 2 - 16 - 4, width: button!.bounds.width - circleDiameter / 2 - 8 * 2, height: 16))
    addressLabel!.textColor = UIColor.whiteColor()
    addressLabel!.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
    addressLabel!.minimumScaleFactor = 0.5
    addressLabel!.adjustsFontSizeToFitWidth = true
    addressLabel!.numberOfLines = 0
    button!.addSubview(addressLabel!)
    addressLabel?.translatesAutoresizingMaskIntoConstraints = false
    let horizontalConstraints2 = NSLayoutConstraint.constraintsWithVisualFormat("H:[address]-(8)-|", options: .AlignAllCenterY, metrics: nil, views: ["address": addressLabel!])
    let leftConstraint2 = NSLayoutConstraint(item: addressLabel!, attribute: .Left, relatedBy: .Equal, toItem: button!, attribute: .Left, multiplier: 1, constant: 58)
    leftConstraint2.priority = 900
    let verticalConstraints2 = NSLayoutConstraint.constraintsWithVisualFormat("V:|->=4-[address(16)]-(2)-[description]", options: .AlignAllCenterX, metrics: nil, views: ["address": addressLabel!, "description": descriptionLabel!])
    button!.addConstraints(horizontalConstraints2)
    button!.addConstraint(leftConstraint2)
    button!.addConstraints(verticalConstraints2)
    
    dateLabel = UILabel(frame: CGRect(x: 58, y: button!.bounds.height / 2 + 10, width: button!.bounds.width - circleDiameter / 2 - 8 * 2, height: 16))
    dateLabel!.textColor = UIColor.whiteColor()
    dateLabel!.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
    button!.addSubview(dateLabel!)
    dateLabel?.translatesAutoresizingMaskIntoConstraints = false
    let horizontalConstraints3 = NSLayoutConstraint.constraintsWithVisualFormat("H:[date]-(8)-|", options: .AlignAllCenterY, metrics: nil, views: ["date": dateLabel!])
    let leftConstraint3 = NSLayoutConstraint(item: dateLabel!, attribute: .Left, relatedBy: .Equal, toItem: button!, attribute: .Left, multiplier: 1, constant: 58)
    leftConstraint3.priority = 900
    let verticalConstraints3 = NSLayoutConstraint.constraintsWithVisualFormat("V:[description]-(2)-[date(16)]->=4-|", options: .AlignAllCenterX, metrics: nil, views: ["date": dateLabel!, "description": descriptionLabel!])
    button!.addConstraints(horizontalConstraints3)
    button!.addConstraint(leftConstraint3)
    button!.addConstraints(verticalConstraints3)
    
    circle = UIImageView(frame: CGRect(x: (350 - circleDiameter) / 2, y: 0, width: circleDiameter, height: circleDiameter))
    circle!.clipsToBounds = true
    circle!.layer.cornerRadius = 50
    circle!.layer.borderWidth = 10
    circle!.layer.borderColor = UIColor(red: 253 / 255, green: 211 / 255, blue: 17 / 255, alpha: 1).CGColor
    circle!.backgroundColor = UIColor(red: 253 / 255, green: 211 / 255, blue: 17 / 255, alpha: 1)
    addSubview(circle!)
  }
  
  func configureWithOffence(offence: Offence, dateFormatter: NSDateFormatter) {
    self.offence = offence
    if let street = offence.street where street.characters.count > 0 {
      addressLabel?.text = street
    } else if let city = offence.city where city.characters.count > 0 {
      addressLabel?.text = city
    } else if let state = offence.state where state.characters.count > 0 {
      addressLabel?.text = state
    }
    descriptionLabel?.text = offence.comment
    dateLabel?.text = dateFormatter.stringFromDate(offence.date)
    if let url = NSURL(string: offence.imageURL) {
      circle?.pin_setImageFromURL(url, placeholderImage: UIImage(named: "img_placeholder"))
    }
  }
  
  func addView() {
    let transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1)
    self.transform = transform
    UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseInOut, animations: {
      self.transform = CGAffineTransformIdentity
      }) { finished in
        self.completeView()
    }
  }
  
  func completeView() {
    button?.hidden = false
    buttonWidthConstraint?.constant = 200
    UIView.animateWithDuration(0.2, delay: 0.0, options: .CurveEaseInOut, animations: {
      self.layoutIfNeeded()
    }) { finished in
      
    }
  }
  
  func removeView() {
    buttonWidthConstraint?.constant = 0
    UIView.animateWithDuration(0.2, delay: 0.0, options: .CurveEaseInOut, animations: {
      self.layoutIfNeeded()
    }) { finished in
      self.button?.hidden = true
      UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseInOut, animations: {
        let transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1)
        self.circle?.image = nil
        self.transform = transform
        }) { finished in
          self.transform = CGAffineTransformIdentity
          self.delegate?.didRemove()
      }
    }
  }
  
  func buttonTapped(sender: UIButton) {
    if let offence = offence {
      delegate?.goToOffence(offence, fromOffenceView: self)
    }
  }
}
