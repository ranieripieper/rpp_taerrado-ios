//
//  LoadingView.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/15/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Async

final class LoadingView: UIView {
  var isAnimating = false
  private var circles = [Circle]()
  
  // MARK: Init
  override init(frame: CGRect) {
    super.init(frame: frame)
    defaultInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    defaultInit()
  }
  
  private func defaultInit() {
    backgroundColor = UIColor.clearColor()
    clipsToBounds = true
  }
  
  // MARK: Animation
  internal func startAnimating() {
    isAnimating = true
    for i in 0 ..< 4 {
      Async.main(after: Double(i) * 0.2) {
        self.addCircle()
      }
    }
  }
  
  internal func stopAnimating(animated: Bool) {
    isAnimating = false
    for circle in circles {
      Async.main(after: Double(circles.indexOf(circle)!) * 0.2) {
        circle.dismiss(animated)
      }
    }
    circles.removeAll(keepCapacity: false)
    setNeedsDisplay()
  }
  
  private func addCircle() {
    if isAnimating {
      let circle = Circle(position: circlePosition())
      circles.append(circle)
      addSubview(circle)
      circle.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, 40, 0)
      UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseInOut, animations: { () -> Void in
        circle.layer.transform = CATransform3DIdentity
        }) { finished in
          circle.addAnimation()
      }
    }
  }
  
  private func circlePosition() -> CGPoint {
    return CGPoint(x: bounds.width / 2 - 46 + CGFloat(circles.count * 28), y: bounds.height / 2 - 10)
  }
}

final class Circle: UIView {
  override init(frame: CGRect) {
    super.init(frame: frame)
    defaultInit()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    defaultInit()
  }
  
  convenience init(position: CGPoint) {
    self.init(frame: CGRect(origin: position, size: CGSize(width: 12, height: 12)))
  }
  
  private func defaultInit() {
    backgroundColor = UIColor.whiteColor()
    layer.cornerRadius = bounds.height / 2
  }
  
  func addAnimation() {
    let animation = CABasicAnimation(keyPath: "transform.translation.y")
    animation.delegate = self
    animation.fromValue = 0
    animation.toValue = 10
    animation.autoreverses = true
    animation.duration = 0.5
    animation.removedOnCompletion = false
    animation.fillMode = kCAFillModeBoth
    animation.repeatCount = Float.infinity
    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
    layer.addAnimation(animation, forKey: "transform.translation.y")
  }
  
  private func dismiss(animated: Bool) {
    layer.removeAnimationForKey("transform.translation.y")
    let currentLayer = layer.presentationLayer() as! CALayer
    layer.transform = currentLayer.transform
    if animated {
      UIView.animateWithDuration(0.3, delay: 0.0, options: .BeginFromCurrentState, animations: {
        self.transform = CGAffineTransformMakeTranslation(0, 40)
      }, completion: { finished in
        self.removeFromSuperview()
      })
    } else {
      removeFromSuperview()
    }
  }
}
