//
//  UploadingView.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/18/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Async

final class UploadingView: UIView {
  @IBOutlet weak var loadingView: LoadingView!
  @IBOutlet weak var uploadErrorButton: UIButton!
  
  var retryCallback: (() -> ())?
  var retryTimer: NSTimer?
  
  class func uploadingView() -> UploadingView {
    return NSBundle.mainBundle().loadNibNamed("UploadingView", owner: self, options: nil).first as! UploadingView
  }
  
  // MARK: Actions
  @IBAction func retry(sender: UIButton) {
    retryCallback?()
  }
  
  // MARK: Animations
  func present() {
    frame = CGRect(x: 0, y: 64, width: UIScreen.mainScreen().bounds.width, height: 36)
    transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -frame.origin.y - bounds.height)
    uploadErrorButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -bounds.height)
    UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
      self.transform = CGAffineTransformIdentity
    }) { finished in
      self.startAnimating()
    }
  }
  
  func dismiss(completion: (() -> ())?) {
    if loadingView.isAnimating {
      loadingView.stopAnimating(true)
      Async.main(after: 0.7) {
        self.internalDismiss(completion)
      }
    } else {
      internalDismiss(completion)
    }
  }
  
  func timerDismiss() {
    internalDismiss(nil)
  }
  
  private func internalDismiss(completion: (() -> ())?) {
    UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
      self.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -self.frame.origin.y - self.bounds.height)
      }, completion: { _ in
        completion?()
    })
  }
  
  func startAnimating() {
    retryTimer?.invalidate()
    retryTimer = nil
    UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
      self.uploadErrorButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -self.bounds.height)
    }) { finished in
      self.loadingView.startAnimating()
    }
  }
  
  func presentError() {
    loadingView.stopAnimating(true)
    UIView.animateWithDuration(0.3, delay: 0.7, options: .CurveEaseInOut, animations: {
      self.uploadErrorButton.transform = CGAffineTransformIdentity
      }, completion: { finished in
        self.retryTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: #selector(UploadingView.timerDismiss), userInfo: nil, repeats: false)
    })
    
  }
}
