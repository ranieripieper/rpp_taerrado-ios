//
//  CustomAlert.swift
//  BeMyChef
//
//  Created by Gilson Gil on 4/21/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class CustomAlert: Alert {
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var messageLabel: UILabel!
  @IBOutlet weak var submessageLabel: UILabel!
  
  @IBAction func okTapped(sender: UIButton) {
    dismiss()
  }
  
  func configureForImageWarning() {
    titleLabel.text = "AVISO"
    messageLabel.text = "Tenha certeza de que a imagem que você vai enviar realmente mostra uma infração de trânsito."
    submessageLabel.text = ""
  }
  
  func configureForForgotPasswordAlert() {
    titleLabel.text = "SENHA"
    messageLabel.text = "Um link foi enviado para você cadastrar uma nova senha."
    submessageLabel.text = ""
  }
}
