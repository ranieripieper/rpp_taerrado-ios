//
//  TriangleView.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class TriangleView: UIView {
  override func drawRect(rect: CGRect) {
    let path = UIBezierPath()
    path.moveToPoint(CGPoint(x: bounds.width / 2, y: 0))
    path.addLineToPoint(CGPoint(x: bounds.width, y: bounds.height))
    path.addLineToPoint(CGPoint(x: 0, y: bounds.height))
    path.closePath()
    UIColor.whiteColor().set()
    path.fill()
  }
}
