//
//  ExpandedMapOffenceView.swift
//  TaErrado
//
//  Created by Gilson Gil on 9/3/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Alamofire
import Async

final class ExpandedMapOffenceView: UIView {
  private var offence: Offence!
  private var imageView: UIImageView!
  private var label: UILabel!
  private var offendedImageView: UIImageView!
  private let originalFrame: CGRect
  private let offendedImageViewSize: CGFloat = 80
  private var leftConstraint: NSLayoutConstraint?
  private var topConstraint: NSLayoutConstraint?
  private var widthConstraint: NSLayoutConstraint?
  private var heightConstraint: NSLayoutConstraint?

  init(frame: CGRect, offence: Offence) {
    originalFrame = frame
    super.init(frame: frame)
    self.offence = offence
    clipsToBounds = true
    translatesAutoresizingMaskIntoConstraints = false
    
    imageView = UIImageView(frame: bounds)
    imageView.layer.borderWidth = 10
    imageView.layer.borderColor = UIColor(red: 253 / 255, green: 211 / 255, blue: 17 / 255, alpha: 1).CGColor
    imageView.clipsToBounds = true
//    imageView.contentMode = .ScaleAspectFill
    imageView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(imageView)
    let imageViewHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-(0)-[imageView]-(0)-|", options: .AlignAllCenterY, metrics: nil, views: ["imageView": imageView])
    let imageViewVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-(0)-[imageView]-(0)-|", options: .AlignAllCenterX, metrics: nil, views: ["imageView": imageView])
    addConstraints(imageViewHorizontalConstraints)
    addConstraints(imageViewVerticalConstraints)
    setImage()
    
    offendedImageView = UIImageView(frame: bounds)
    offendedImageView.image = UIImage(named: "img_placeholder")
    offendedImageView.clipsToBounds = true
    offendedImageView.contentMode = .ScaleAspectFill
    offendedImageView.layer.cornerRadius = 40
    offendedImageView.translatesAutoresizingMaskIntoConstraints = false
    offendedImageView.layer.borderWidth = 5
    offendedImageView.layer.borderColor = UIColor.whiteColor().CGColor
    offendedImageView.alpha = 0
    addSubview(offendedImageView)
    let offendedImageViewCenterConstraint = NSLayoutConstraint(item: offendedImageView, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0)
    let offendedImageViewWidthConstraint = NSLayoutConstraint(item: offendedImageView, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: offendedImageViewSize)
    let offendedImageViewVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[imageView]-(30)-|", options: .AlignAllCenterX, metrics: nil, views: ["imageView": offendedImageView])
    let offendedImageHeightConstraint = NSLayoutConstraint(item: offendedImageView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: offendedImageViewSize)
    offendedImageHeightConstraint.priority = 900
    addConstraints([offendedImageViewCenterConstraint, offendedImageViewWidthConstraint])
    addConstraints(offendedImageViewVerticalConstraints)
    offendedImageView.addConstraint(offendedImageHeightConstraint)
    request(.GET, offence.offendedImageURL)
      .validate(contentType: ["image/*"])
      .responseImage { response in
        switch response.result {
        case .Success(let image):
          Async.main {
            self.offendedImageView.image = image
          }
        case .Failure:
          break
        }
    }
    
    label = UILabel(frame: bounds)
    label.numberOfLines = 0
    label.textColor = UIColor.whiteColor()
    label.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
    label.textAlignment = .Center
    label.translatesAutoresizingMaskIntoConstraints = false
    addSubview(label)
    let labelHorizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-(16)-[label]-(16)-|", options: .AlignAllCenterY, metrics: nil, views: ["label": label])
    let labelVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-(16)-[label]-(16)-[imageView]", options: .AlignAllCenterX, metrics: nil, views: ["label": label, "imageView": offendedImageView])
    addConstraints(labelHorizontalConstraints)
    addConstraints(labelVerticalConstraints)
  }

  required init?(coder aDecoder: NSCoder) {
    originalFrame = CGRect.null
    super.init(coder: aDecoder)
  }
  
  private func setImage() {
    if let image = offence.image {
      drawImage(image)
    } else {
      drawImage(UIImage(named: "img_placeholder")!)
      request(.GET, offence.imageURL)
        .validate(contentType: ["image/*"])
        .responseImage { response in
          switch response.result {
          case .Success(let image):
            self.offence.image = image
            self.drawImage(image)
          case .Failure:
            break
          }
      }
    }
  }
  
  private func drawImage(image: UIImage) {
    imageView.image = image.feedBlurredImage()
  }
  
  static func presentInView(view: UIView, fromRect rect: CGRect, withOffence offence: Offence) -> ExpandedMapOffenceView {
    let expandedMapOffenceView = ExpandedMapOffenceView(frame: rect, offence: offence)
    expandedMapOffenceView.frame = rect
    expandedMapOffenceView.layer.cornerRadius = 50
    view.addSubview(expandedMapOffenceView)
    expandedMapOffenceView.leftConstraint = NSLayoutConstraint(item: expandedMapOffenceView, attribute: .Left, relatedBy: .Equal, toItem: view, attribute: .Left, multiplier: 1, constant: rect.origin.x)
    expandedMapOffenceView.topConstraint = NSLayoutConstraint(item: expandedMapOffenceView, attribute: .Top, relatedBy: .Equal, toItem: view, attribute: .Top, multiplier: 1, constant: rect.origin.y)
    expandedMapOffenceView.widthConstraint = NSLayoutConstraint(item: expandedMapOffenceView, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: rect.width)
    expandedMapOffenceView.heightConstraint = NSLayoutConstraint(item: expandedMapOffenceView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: rect.height)
    view.addConstraint(expandedMapOffenceView.leftConstraint!)
    view.addConstraint(expandedMapOffenceView.topConstraint!)
    view.addConstraint(expandedMapOffenceView.widthConstraint!)
    view.addConstraint(expandedMapOffenceView.heightConstraint!)
    view.layoutIfNeeded()
    let size = min(UIScreen.mainScreen().bounds.width, UIScreen.mainScreen().bounds.height) - 2 * 20
    let frame = CGRect(x: (UIScreen.mainScreen().bounds.width - size) / 2, y: (UIScreen.mainScreen().bounds.height - size) / 2, width: size, height: size)
    expandedMapOffenceView.leftConstraint?.constant = frame.origin.x
    expandedMapOffenceView.topConstraint?.constant = frame.origin.y
    expandedMapOffenceView.widthConstraint?.constant = frame.width
    expandedMapOffenceView.heightConstraint?.constant = frame.height
    UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
      expandedMapOffenceView.imageView.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: frame.width, height: frame.height))
      view.layoutIfNeeded()
      expandedMapOffenceView.layer.cornerRadius = 5
      }, completion: { _ in
        expandedMapOffenceView.label.text = offence.comment
        expandedMapOffenceView.label.alpha = 0
        UIView.animateWithDuration(0.3) {
          expandedMapOffenceView.label.alpha = 1
          expandedMapOffenceView.offendedImageView.alpha = 1
        }
    })
    return expandedMapOffenceView
  }
  
  func dismiss(completion: () -> ()) {
    offendedImageView.alpha = 0
    leftConstraint?.constant = originalFrame.origin.x
    topConstraint?.constant = originalFrame.origin.y
    widthConstraint?.constant = originalFrame.width
    heightConstraint?.constant = originalFrame.height
    UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
      self.layoutIfNeeded()
      self.imageView.layer.cornerRadius = 50
      self.layer.cornerRadius = 50
      self.label.alpha = 0
      }, completion: { _ in
        self.removeFromSuperview()
        completion()
    })
  }
}
