//
//  RefreshView.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/18/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Async

class RefreshView: UIView {
  let circle1: Circle
  let circle2: Circle
  let circle3: Circle
  let circle4: Circle
  
  var isAnimating = false
  
  override init(frame: CGRect) {
    circle1 = Circle(position: CGPoint(x: frame.width / 2 - 6 - 12 - 12 - 12, y: 20))
    circle2 = Circle(position: CGPoint(x: frame.width / 2 - 6 - 12, y: 20))
    circle3 = Circle(position: CGPoint(x: frame.width / 2 + 6, y: 20))
    circle4 = Circle(position: CGPoint(x: frame.width / 2 + 6 + 12 + 12, y: 20))
    
    super.init(frame: frame)
    
    addSubview(circle1)
    addSubview(circle2)
    addSubview(circle3)
    addSubview(circle4)
    
    circle1.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, 100, 0)
    circle2.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, 100, 0)
    circle3.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, 100, 0)
    circle4.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, 100, 0)
  }

  required init?(coder aDecoder: NSCoder) {
    circle1 = Circle(position: CGPoint(x: 0, y: 0))
    circle2 = Circle(position: CGPoint(x: 0, y: 0))
    circle3 = Circle(position: CGPoint(x: 0, y: 0))
    circle4 = Circle(position: CGPoint(x: 0, y: 0))
    
    super.init(coder: aDecoder)
  }
  
  func updateWithProgress(progress: CGFloat) {
    if isAnimating {
      return
    }
    if progress > 0.88 {
      startAnimating()
    }
    let c1: CGFloat = min(max(0, progress) / 0.4, 1)
    let c2: CGFloat = min(max(0, progress) / 0.6, 1)
    let c3: CGFloat = min(max(0, progress) / 0.8, 1)
    let c4: CGFloat = min(max(0, progress) / 1, 1)
    
    circle1.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, (1 - c1) * 100, 0)
    circle2.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, (1 - c2) * 100, 0)
    circle3.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, (1 - c3) * 100, 0)
    circle4.layer.transform = CATransform3DTranslate(CATransform3DIdentity, 0, (1 - c4) * 100, 0)
  }
  
  func startAnimating() {
    isAnimating = true
    circle1.addAnimation()
    Async.main(after: 0.2) {
      self.circle2.addAnimation()
    }
    Async.main(after: 0.4) {
      self.circle3.addAnimation()
    }
    Async.main(after: 0.6) {
      self.circle4.addAnimation()
    }
  }
  
  internal func stopAnimating(animated: Bool) {
    isAnimating = false
    circle1.layer.removeAllAnimations()
    circle2.layer.removeAllAnimations()
    circle3.layer.removeAllAnimations()
    circle4.layer.removeAllAnimations()
  }
}
