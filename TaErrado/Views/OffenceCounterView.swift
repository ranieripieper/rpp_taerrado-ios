//
//  OffenceCounterView.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

final class OffenceCounterView: UIView {
  @IBOutlet private weak var counterLabel: UILabel!
  
  private var count = 0
  
  private lazy var numberFormatter: NSNumberFormatter = {
    let numberFormatter = NSNumberFormatter()
    numberFormatter.minimumIntegerDigits = 2
    numberFormatter.locale = NSLocale(localeIdentifier: "pt-BR")
    return numberFormatter
  }()
  
  func configureWithCount(count: Int) {
    self.count = count
    counterLabel.text = numberFormatter.stringFromNumber(NSNumber(integer: count))
  }
  
  func increment() {
    count += 1
    counterLabel.text = numberFormatter.stringFromNumber(NSNumber(integer: count))
  }
}
