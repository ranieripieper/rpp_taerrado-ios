//
//  TwoButtonAlert.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Async

class TwoButtonAlert: Alert {
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var messageLabel: UILabel!
  @IBOutlet weak var responseLabel: UILabel!
  @IBOutlet weak var noButton: UIButton!
  @IBOutlet weak var yesButton: UIButton!
  
  var callback: (() -> ())?
  var isLogin = false
  
  // MARK: Configuration
  func configureForLogin() {
    titleLabel.text = "Conecte-se!"
    messageLabel.text = "Para continuar com esta ação, você precisa se conectar no Facebook.\nDeseja se conectar agora?"
    responseLabel.text = ""
    isLogin = true
  }
  
  func configureForDeleteAccount() {
    titleLabel.text = "MESMO? :("
    messageLabel.text = "Vamos sentir falta da sua ajuda para mudarmos nossa cidade..\n\nTem certeza que quer ir?"
    responseLabel.text = ""
  }
  
  // MARK: Actions
  @IBAction func noTapped(sender: UIButton) {
    dismiss()
  }
  
  @IBAction func yesTapped(sender: UIButton) {
    if isLogin {
      self.callback?()
      self.dismiss()
    } else {
      UIView.animateWithDuration(0.3, animations: { () -> Void in
        self.messageLabel.alpha = 0
        self.noButton.alpha = 0
        self.yesButton.alpha = 0
        self.responseLabel.alpha = 1
        }) { finished in
          Async.main(after: 2) {
            self.callback?()
            self.dismiss()
          }
      }
    }
  }
}
