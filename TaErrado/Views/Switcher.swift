//
//  Switcher.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/16/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

enum State {
  case On, Off
}

final class Switcher: UIView {
  @IBOutlet weak var containerViewLeftConstraint: NSLayoutConstraint!
  @IBOutlet weak var viewController: UIViewController!

  var state: State = .Off
  var callback: (Bool -> ())?
  lazy var facebook: Facebook = {
    return Facebook()
  }()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    let tap = UITapGestureRecognizer(target: self, action: #selector(Switcher.toggle(_:)))
    addGestureRecognizer(tap)
    let on = NSUserDefaults.standardUserDefaults().boolForKey("FacebookShare") && facebook.hasPermissions()
    if on {
      turnOn(false)
    } else {
      turnOff(false)
    }
  }
  
  func toggle(sender: UITapGestureRecognizer) {
    switch state {
    case .Off:
      facebook.askPublishPermissionsFromViewController(viewController) {
        switch $0 {
        case .Success:
          self.turnOn(true)
        case .Failure:
          break
        }
      }
    case .On:
      turnOff(true)
    }
  }
  
  func turnOff(animated: Bool) {
    state = .Off
    containerViewLeftConstraint.constant = -36
    let duration: NSTimeInterval = animated ? 0.3 : 0
    UIView.animateWithDuration(duration) {
      self.layoutIfNeeded()
      NSUserDefaults.standardUserDefaults().removeObjectForKey("FacebookShare")
      NSUserDefaults.standardUserDefaults().synchronize()
    }
  }
  
  func turnOn(animated: Bool) {
    state = .On
    containerViewLeftConstraint.constant = 0
    let duration: NSTimeInterval = animated ? 0.3 : 0
    UIView.animateWithDuration(duration) {
      self.layoutIfNeeded()
      NSUserDefaults.standardUserDefaults().setBool(true, forKey: "FacebookShare")
      NSUserDefaults.standardUserDefaults().synchronize()
    }
  }
}
