//
//  FeedViewController.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/15/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Async

final class FeedViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var feedCollectionDelegate: FeedCollectionDelegate!
  
  weak var aNavigationController: NavigationController? {
    return navigationController as? NavigationController
  }
  let refreshControl = UIRefreshControl()
  weak var refreshView: RefreshView?
  var isRefreshIconsOverlap = false
  var isRefreshAnimating = false
  var offences = [Offence]()
  var currentPage = 0
  var loading = false
  var endReached = false
  var adMobView: GADBannerView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(UINib(nibName: "FeedCell", bundle: nil), forCellReuseIdentifier: "FeedCell")
    tableView.estimatedRowHeight = 400
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
    view.layoutIfNeeded()
    setupRefreshControl()
    adMob()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    feedCollectionDelegate.delegate = self
    tableView.dataSource = feedCollectionDelegate
    tableView.delegate = feedCollectionDelegate
    currentPage = 0
    endReached = false
    loading = false
    getOffences()
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(FeedViewController.postedOffence(_:)), name: PostOffenceNotification, object: nil)
    
    DDSAnalytics.trackScreen("Feed", withContentId: nil)
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    tableView.dataSource = nil
    tableView.delegate = nil
    feedCollectionDelegate.delegate = nil
    NSNotificationCenter.defaultCenter().removeObserver(self, name: PostOffenceNotification, object: nil)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if let profileViewController = segue.destinationViewController as? ProfileViewController, let offence = sender as? Offence {
      profileViewController.userId = offence.offendedId
    }
  }
  
  // MARK: Menu
  @IBAction func toggleMenu(sender: UIButton) {
    aNavigationController?.toggleMenu(nil)
  }
  
  @IBAction func goToCamera(sender: UIButton) {
    aNavigationController?.goToCamera()
  }
  
  // MARK: API
  func getOffences() {
    if loading || endReached {
      return
    }
    loading = true
    Offence.feed(currentPage + 1) {
      switch $0 {
      case .Success(let data):
        self.endReached = data.count == 0
        self.currentPage += 1
        if self.currentPage == 1 {
          self.offences = data
        } else {
          self.offences += data
        }
        self.tableView.reloadData()
      case .Failure:
        break
      }
      self.loading = false
      self.refreshControl.endRefreshing()
      self.isRefreshAnimating = false
      self.refreshView?.stopAnimating(false)
    }
  }
  
  // MARK: Refresh Control
  func setupRefreshControl() {
    tableView.insertSubview(refreshControl, atIndex: 0)
    
    // Setup the loading view, which will hold the moving graphics
    let refreshView = RefreshView(frame: CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width, height: 150))
    refreshView.backgroundColor = UIColor.clearColor()
    
    // Clip so the graphics don't stick out
    refreshView.clipsToBounds = true;
    
    // Hide the original spinner icon
    refreshControl.tintColor = UIColor.clearColor()
    
    // Add the loading and colors views to our refresh control
    refreshControl.insertSubview(refreshView, atIndex: 0)
    
    self.refreshView = refreshView
    
    // Initalize flags
    isRefreshIconsOverlap = false;
    isRefreshAnimating = false;
    
    // When activated, invoke our refresh function
    refreshControl.addTarget(self, action: #selector(FeedViewController.refresh), forControlEvents: UIControlEvents.ValueChanged)
  }
  
  func refresh(){
    currentPage = 0
    endReached = false
    loading = false
    getOffences()
  }
  
  func adMob() {
    Async.main(after: 1) {
      let bannerWidth: CGFloat = 320
      let bannerHeight: CGFloat = 50
      self.adMobView = GADBannerView(frame: CGRect(x: (UIScreen.mainScreen().bounds.width - bannerWidth) / 2, y: UIScreen.mainScreen().bounds.height - bannerHeight, width: bannerWidth, height: bannerHeight))
      self.adMobView.rootViewController = self
      self.adMobView.adUnitID = AdMob.unitId
      self.adMobView.loadRequest(AdMob.request())
      self.view.addSubview(self.adMobView)
      self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[ad(50)]-0-|", options: [], metrics: nil, views: ["ad": self.adMobView]))
      self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[ad(320)]", options: [], metrics: nil, views: ["ad": self.adMobView]))
      self.view.addConstraint(NSLayoutConstraint(item: self.adMobView, attribute: .CenterX, relatedBy: .Equal, toItem: self.view, attribute: .CenterX, multiplier: 1, constant: 0))
    }
  }
  
  func postedOffence(notification: NSNotification) {
    refresh()
//    guard let userInfo = notification.userInfo, offence = userInfo["offence"] as? Offence else {
//      
//      return
//    }
//    offences.insert(offence, atIndex: 0)
//    let firstRow = NSIndexPath(forRow: 0, inSection: 0)
//    tableView.insertRowsAtIndexPaths([firstRow], withRowAnimation: .Automatic)
//    tableView.scrollToRowAtIndexPath(firstRow, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self, name: PostOffenceNotification, object: nil)
  }
}

extension FeedViewController { // Feed Cell Delegate
  func reportCell(cell: FeedCell) {
    let twoButtonAlert = NSBundle.mainBundle().loadNibNamed("TwoButtonAlert", owner: self, options: nil).first as! TwoButtonAlert
    twoButtonAlert.present()
    twoButtonAlert.callback = {
      if let indexPath = self.tableView.indexPathForCell(cell) {
        let offence = self.offences[indexPath.row]
        offence.report() {
          switch $0 {
          case .Success:
            offence.reported = true
            cell.updateReported(offence)
          case .Failure:
            break
          }
        }
      }
    }
  }
  
  func goToUserCell(cell: FeedCell) {
    if let indexPath = tableView.indexPathForCell(cell) {
      let offence = offences[indexPath.row]
      performSegueWithIdentifier("SegueProfile", sender: offence)
    }
  }
  
  func swearCell(cell: FeedCell, on: Bool) {
    cell.swearButton.selected = !cell.swearButton.selected
    if let indexPath = self.tableView.indexPathForCell(cell) {
      let offence = self.offences[indexPath.row]
      if on {
        if offence.swearings == 0 {
          cell.swearingsLabel.text = "1 xingamento"
        } else {
          cell.swearingsLabel.text = "\(offence.swearings + 1) xingamentos"
        }
      } else {
        if offence.swearings == 2 {
          cell.swearingsLabel.text = "1 xingamento"
        } else {
          cell.swearingsLabel.text = "\(offence.swearings - 1) xingamentos"
        }
      }
      offence.sweared = on
      offence.swear(on) {
        switch $0 {
        case .Success(let data):
          offence.swearings = data
          offence.sweared = on
          cell.updateSwearings(offence)
        case .Failure:
          break
        }
      }
    }
  }
}

extension FeedViewController: FeedCollectionDelegateDelegate {
  func didScroll(scrollView: UIScrollView) {
    let progress = (-scrollView.contentOffset.y - 64) / 150
    refreshView?.updateWithProgress(progress)
    // If we're refreshing and the animation is not playing, then play the animation
    if (refreshControl.refreshing && !isRefreshAnimating) {
      refreshView?.startAnimating()
      isRefreshAnimating = true
    }
  }
  
  func didReachEnd() {
    getOffences()
  }
}
