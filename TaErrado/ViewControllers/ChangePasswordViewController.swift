//
//  ChangePasswordViewController.swift
//  TaErrado
//
//  Created by Gilson Gil on 7/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Alamofire
import Async

class ChangePasswordViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var passwordConfirmationTextField: UITextField!
  @IBOutlet weak var loadingView: LoadingView!
  @IBOutlet weak var widthConstraint: NSLayoutConstraint!
  
  var adMobView: GADBannerView!
  var user: User?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    widthConstraint.constant = UIScreen.mainScreen().bounds.width - 60 * 2
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    avatarImageView.layer.borderColor = UIColor(red: 253 / 255, green: 211 / 255, blue: 17 / 255, alpha: 1).CGColor
    let URLString: String
    if let user = user, avatarURL = user.avatarURL where avatarURL.characters.count > 0 {
      URLString = avatarURL
    } else if let avatarURL = NSUserDefaults.standardUserDefaults().objectForKey("H!*@(H#@N!&*T#&*GUI!") as? String {
      URLString = avatarURL
    } else {
      URLString = ""
    }
    request(.GET, URLString)
      .validate(contentType: ["image/*"])
      .responseImage { response in
        switch response.result {
        case .Success(let image):
          self.avatarImageView.image = image
        case .Failure:
          break
        }
    }
    adMob()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    DDSAnalytics.trackScreen("ChangePassword", withContentId: nil)
  }
  
  // MARK: Save
  @IBAction func saveTapped() {
    if validateFields() {
      loadingView.startAnimating()
      Manager.sharedInstance.request(NetworkManager.Router.ChangePassword(passwordTextField.text!))
        .responseAPI { response in
          switch response.result {
          case .Success:
            self.loadingView.stopAnimating(true)
            Async.main(after: 0.8) {
              self.navigationController?.popViewControllerAnimated(true)
            }
          case .Failure(let error):
            let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { _ in
              self.loadingView.stopAnimating(true)
            }))
            self.presentViewController(alert, animated: true, completion: nil)
          }
      }
    }
  }
  
  func validateFields() -> Bool {
    return passwordTextField.text!.characters.count > 0 && passwordTextField.text! == passwordConfirmationTextField.text!
  }
  
  func adMob() {
    Async.main(after: 1) {
      let bannerWidth: CGFloat = 320
      let bannerHeight: CGFloat = 50
      self.adMobView = GADBannerView(frame: CGRect(x: (UIScreen.mainScreen().bounds.width - bannerWidth) / 2, y: UIScreen.mainScreen().bounds.height - bannerHeight, width: bannerWidth, height: bannerHeight))
      self.adMobView.rootViewController = self
      self.adMobView.adUnitID = AdMob.unitId
      self.adMobView.loadRequest(AdMob.request())
      self.view.addSubview(self.adMobView)
      self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[ad(50)]-0-|", options: [], metrics: nil, views: ["ad": self.adMobView]))
      self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[ad(320)]", options: [], metrics: nil, views: ["ad": self.adMobView]))
      self.view.addConstraint(NSLayoutConstraint(item: self.adMobView, attribute: .CenterX, relatedBy: .Equal, toItem: self.view, attribute: .CenterX, multiplier: 1, constant: 0))
    }
  }
}
