//
//  NavigationController.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/15/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

final class NavigationController: UINavigationController {
  var locationManager = LocationManager()
  var profile = false
  private var uploadingView: UploadingView?
  
  private var menuViewController: MenuViewController?
  private var mapViewController: MapViewController?
  private var feedViewController: FeedViewController!
  private var profileViewController: ProfileViewController!
  
  let loggedFbId = NSUserDefaults.standardUserDefaults().stringForKey("HAUHEUHRU331rfds")
  var facebook: Facebook?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    locationManager.locationManagerDelegate = self
    if !profile {
      goToMap()
    }
  }
  
  // MARK: Menu
  func toggleMenu(completion: (() -> ())?) {
    if menuViewController == nil {
      presentMenu()
    } else {
      dismissMenu(completion)
    }
  }
  
  private func presentMenu() {
    menuViewController = storyboard?.instantiateViewControllerWithIdentifier("MenuViewController") as? MenuViewController
    menuViewController!.aNavigationController = self
    menuViewController!.willMoveToParentViewController(self)
    menuViewController!.view.frame = view.bounds
    view.insertSubview(menuViewController!.view, belowSubview: navigationBar)
    menuViewController!.didMoveToParentViewController(self)
  }
  
  func dismissMenu(completion: (() -> ())?) {
    if let menuViewController = menuViewController {
      menuViewController.dismiss() { finished in
        self.menuViewController?.removeFromParentViewController()
        self.menuViewController?.view.removeFromSuperview()
        self.menuViewController = nil
        completion?()
      }
    } else {
      completion?()
    }
  }
  
  func goToMap() {
    if mapViewController == nil {
      mapViewController = storyboard?.instantiateViewControllerWithIdentifier("MapViewController") as? MapViewController
    }
    if let mapViewController = mapViewController {
      viewControllers = [mapViewController]
    }
    dismissMenu(nil)
  }
  
  func goToFeed() {
    if feedViewController == nil {
      feedViewController = storyboard?.instantiateViewControllerWithIdentifier("FeedViewController") as! FeedViewController
    }
    viewControllers = [feedViewController]
    dismissMenu(nil)
  }
  
  func goToProfile() {
    if profileViewController == nil {
      profileViewController = storyboard?.instantiateViewControllerWithIdentifier("ProfileViewController") as! ProfileViewController
      let menuBarButton = UIBarButtonItem(image: UIImage(named: "icn_menu"), style: .Plain, target: profileViewController, action: #selector(NavigationController.toggleMenu(_:)))
      menuBarButton.tintColor = UIColor(red: 64 / 255, green: 64 / 255, blue: 65 / 255, alpha: 1)
      profileViewController.navigationItem.leftBarButtonItems = [menuBarButton]
    }
    viewControllers = [profileViewController]
    dismissMenu(nil)
  }
  
  func goToCamera() {
    dismissMenu() {
      let cameraNavigationController = self.storyboard?.instantiateViewControllerWithIdentifier("CameraNavigationController") as! UINavigationController
      if let cameraViewController = cameraNavigationController.viewControllers.first as? CameraViewController {
        cameraViewController.aNavigationController = self
      }
      self.presentViewController(cameraNavigationController, animated: true, completion: nil)
    }
  }
  
  func dismissCamera(completion: (() -> ())?) {
    dismissViewControllerAnimated(true, completion: completion)
  }
  
  func signout() {
    uploadingView = nil
    menuViewController = nil
    mapViewController = nil
    feedViewController = nil
    profileViewController = nil
    User.removeUser()
    NetworkManager.Router.authToken = nil
    (UIApplication.sharedApplication().delegate as! AppDelegate).goToLogin()
  }
  
  // MARK: Upload
  func upload(offence: Offence) {
    dismissCamera() {
      if NSUserDefaults.standardUserDefaults().boolForKey("FacebookShare") {
        self.facebook = Facebook()
        self.facebook?.postImage(offence.image!, message: offence.comment)
        DDSAnalytics.trackShareWithNetwork("Facebook", action: "Share", target: "\(offence.id) - \(offence.imageURL)")
      }
      self.uploadingView = UploadingView.uploadingView()
      self.view.insertSubview(self.uploadingView!, belowSubview: self.navigationBar)
      self.uploadingView?.present()
      self.uploadingView?.retryCallback = {
        self.uploadingView?.startAnimating()
        self.internalUpload(offence)
      }
      self.internalUpload(offence)
    }
  }
  
  private func internalUpload(offence: Offence) {
    offence.upload() {
      switch $0 {
      case .Success:
        self.uploadingView?.dismiss() {
          self.uploadingView?.removeFromSuperview()
          self.uploadingView = nil
        }
//        if let feedViewController = self.feedViewController {
//          feedViewController.offences.insert(data, atIndex: 0)
//          feedViewController.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: .Automatic)
//          feedViewController.tableView.setContentOffset(feedViewController.tableView.bounds.origin, animated: true)
//        }
      case .Failure:
        self.uploadingView?.presentError()
      }
    }
  }
}

extension NavigationController: LocationManagerDelegate {
  func didUpdateLocation(userLocation: CLLocation) {
    mapViewController?.centerMapAtLocation(userLocation.coordinate)
  }
  
  func didRespondToRequestWithStatus(status: CLAuthorizationStatus) {
    
  }
}
