//
//  PictureDetailsViewController.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/16/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Async

final class PictureDetailsViewController: UIViewController {
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var textView: UITextView!
  @IBOutlet weak var switcher: Switcher!
  @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
  
  var aNavigationController: NavigationController!
  var offence: Offence?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    imageView.image = offence!.image
    processImage()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    textView.becomeFirstResponder()
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PictureDetailsViewController.keyboardChange(_:)), name: UIKeyboardWillChangeFrameNotification, object: nil)
    
    DDSAnalytics.trackScreen("SendPicture", withContentId: nil)
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillChangeFrameNotification, object: nil)
    textView.resignFirstResponder()
  }
  
  func keyboardChange(notification: NSNotification) {
    if let frameValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
      bottomConstraint.constant = frameValue.CGRectValue().height
    }
  }
  
  func processImage() {
    Async.background {
      let image = self.offence!.image?.postBlurredImage()
      Async.main {
        self.imageView.image = image
      }
    }
  }
  
  // MARK: Send
  @IBAction func sendTapped(sender: UIBarButtonItem) {
    offence!.comment = textView.text
    if let loggedFbId = aNavigationController.loggedFbId {
      offence!.offendedId = Int(loggedFbId) ?? 0
    } else {
      offence!.offendedId = 0
    }
    aNavigationController.upload(offence!)
  }
  
  deinit {
    textView.delegate = nil
  }
}
