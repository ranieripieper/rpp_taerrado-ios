//
//  MapViewController.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/15/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import MapKit
import Async

let PostOffenceNotification = "PostOffenceNotification"

final class MapViewController: UIViewController {
  @IBOutlet weak var mapView: MKMapView?
  @IBOutlet weak var mapViewDelegate: MapViewDelegate? {
    didSet {
      mapViewDelegate?.delegate = self
    }
  }
  @IBOutlet weak var offenceCounterView: OffenceCounterView!
  @IBOutlet weak var offenceCounterViewBottomConstraint: NSLayoutConstraint!
  weak var expandedMapOffenceView: ExpandedMapOffenceView?
  
  weak var aNavigationController: NavigationController? {
    return navigationController as? NavigationController
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    centerMapAtLocation(CLLocationCoordinate2D(latitude: CLLocationDegrees(LocationManager.latitude), longitude: CLLocationDegrees(LocationManager.longitude)))
    getOffences(nil)
    getOffenceCount()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    mapViewDelegate?.delegate = self
    mapView?.delegate = mapViewDelegate
    getOffences(nil)
    getOffenceCount()
    
    DDSAnalytics.trackScreen("Map", withContentId: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MapViewController.postedOffence(_:)), name: PostOffenceNotification, object: nil)
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    mapViewDelegate?.delegate = nil
    mapView?.delegate = nil
    NSNotificationCenter.defaultCenter().removeObserver(self, name: PostOffenceNotification, object: nil)
  }
  
  // MARK: Menu
  @IBAction func toggleMenu(sender: UIButton) {
    aNavigationController?.toggleMenu(nil)
  }
  
  @IBAction func goToCamera(sender: UIButton) {
    aNavigationController?.goToCamera()
  }
  
  // MARK: Map
  func centerMapAtLocation(location: CLLocationCoordinate2D) {
    mapView?.region = MKCoordinateRegion(center: location, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
  }
  
  // MARK: API
  func getOffences(coordinate: CLLocationCoordinate2D?) {
    if let coordinate = coordinate {
      Offence.coordinate(coordinate) {
        switch $0 {
        case .Success(let data):
          let ids = self.mapView?.annotations.filter {
            $0 is Offence
            }.map {
              ($0 as! Offence).id
          }
          let offences = data.filter {
            ids?.indexOf($0.id) == nil
          }
          self.mapView?.addAnnotations(offences)
        case .Failure:
          break
        }
      }
    } else {
      Offence.near {
        switch $0 {
        case .Success(let data):
          self.mapView?.addAnnotations(data)
        case .Failure:
          break
        }
      }
    }
  }
  
  // MARK: Offence Counter View
  func getOffenceCount() {
    Offence.count {
      switch $0 {
      case .Success(let data):
        self.showCounter(data)
      case .Failure:
        break
      }
    }
  }
  
  func showCounter(count: Int) {
    Async.main {
      self.offenceCounterView.configureWithCount(count)
      self.offenceCounterView.layoutIfNeeded()
      self.offenceCounterViewBottomConstraint.constant = 0
      UIView.animateWithDuration(0.5) {
        self.view.layoutIfNeeded()
      }
    }
  }
  
  func postedOffence(notification: NSNotification) {
    guard let userInfo = notification.userInfo, offence = userInfo["offence"] as? Offence else {
      
      return
    }
    
    mapView?.addAnnotation(offence)
    offenceCounterView.increment()
  }
  
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesEnded(touches, withEvent: event)
    expandedMapOffenceView?.dismiss { [weak self] in
      self?.expandedMapOffenceView = nil
    }
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self, name: PostOffenceNotification, object: nil)
    mapViewDelegate?.delegate = nil
    mapView?.delegate = nil
  }
}

extension MapViewController: MapViewDelegateDelegate {
  func regionDidChange(center: CLLocationCoordinate2D) {
    getOffences(center)
  }
  
  func goToOffence(offence: Offence, fromOffenceView offenceView: OffenceView) {
    let rect = view.convertRect(offenceView.circleFrame, fromView: offenceView)
    guard expandedMapOffenceView == nil else {
      return
    }
    expandedMapOffenceView = ExpandedMapOffenceView.presentInView(view, fromRect: rect, withOffence: offence)
  }
}
