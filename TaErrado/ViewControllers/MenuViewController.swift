//
//  MenuViewController.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/16/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Async

final class MenuViewController: UIViewController {
  @IBOutlet weak var mapButton: UIButton!
  @IBOutlet weak var feedButton: UIButton? {
    didSet {
      removeFeedButton()
    }
  }
  @IBOutlet weak var profileButton: UIButton!
  @IBOutlet weak var signoutButton: UIButton!
  
  var aNavigationController: NavigationController!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    present()
    
    DDSAnalytics.trackScreen("Menu", withContentId: nil)
  }
  
  func removeFeedButton() {
    guard let user = User.persistedUser() where user.superuser else {
      feedButton?.removeFromSuperview()
      
      return
    }
  }
  
  // MARK: Button Animations
  func moveButton(button: UIButton, down: Bool) {
    if down {
      button.transform = CGAffineTransformIdentity
    } else {
      var transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -button.bounds.height - 64 - 20)
      transform = CGAffineTransformRotate(transform, -CGFloat(M_PI))
      button.transform = transform
    }
  }
  
  func moveButton(button: UIButton, down: Bool, animated: Bool, completion: (Bool -> ())?) {
    if animated {
      UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
        self.moveButton(button, down: down)
        }, completion: completion)
    } else {
      self.moveButton(button, down: down)
    }
  }
  
  func moveButtons(down: Bool, animated: Bool, completion: (Bool -> ())?) {
    moveButton(mapButton, down: down, animated: animated, completion: nil)
    if let feedButton = feedButton {
      moveButton(feedButton, down: down, animated: animated, completion: nil)
    }
    moveButton(profileButton, down: down, animated: animated, completion: nil)
    moveButton(signoutButton, down: down, animated: animated, completion: completion)
  }
  
  func present() {
    moveButtons(false, animated: false, completion: nil)
    UIView.animateWithDuration(0.6) {
      self.view.backgroundColor = UIColor(white: 0, alpha: 0.5)
    }
    moveButton(mapButton, down: true, animated: true, completion: nil)
    if let feedButton = feedButton {
      Async.main(after: 0.1) {
        self.moveButton(feedButton, down: true, animated: true, completion: nil)
      }
    }
    Async.main(after: self.feedButton == nil ? 0.1 : 0.2) {
      self.moveButton(self.profileButton, down: true, animated: true, completion: nil)
    }
    Async.main(after: self.feedButton == nil ? 0.2 : 0.3) {
      self.moveButton(self.signoutButton, down: true, animated: true, completion: nil)
    }
  }
  
  func dismiss(completion: (Bool -> ())?) {
    UIView.animateWithDuration(0.6) {
      self.view.backgroundColor = UIColor(white: 0, alpha: 0)
    }
    moveButton(mapButton, down: false, animated: true, completion: nil)
    if let feedButton = feedButton {
      Async.main(after: 0.1) {
        self.moveButton(feedButton, down: false, animated: true, completion: nil)
      }
    }
    Async.main(after: self.feedButton == nil ? 0.1 : 0.2) {
      self.moveButton(self.profileButton, down: false, animated: true, completion: nil)
    }
    Async.main(after: self.feedButton == nil ? 0.2 : 0.3) {
      self.moveButton(self.signoutButton, down: false, animated: true, completion: completion)
    }
  }
  
  // MARK: Actions
  @IBAction func goToMap(sender: UIButton) {
    aNavigationController.goToMap()
  }
  
  @IBAction func goToFeed(sender: UIButton) {
    aNavigationController.goToFeed()
  }
  
  @IBAction func goToProfile(sender: UIButton) {
    aNavigationController.goToProfile()
  }
  
  @IBAction func signout(sender: UIButton) {
    aNavigationController.signout()
  }
  
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesEnded(touches, withEvent: event)
    aNavigationController.dismissMenu(nil)
  }
}
