//
//  LoginViewController.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/15/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Alamofire
import Crashlytics
import Async

final class LoginViewController: UIViewController {
  @IBOutlet weak var logoImageView: UIImageView!
  @IBOutlet weak var backgroundImageView: UIImageView!
  @IBOutlet weak var loadingView: LoadingView!
  @IBOutlet weak var bgView: UIView!
  @IBOutlet weak var loginLabel: UILabel!
  @IBOutlet weak var loginTextField: UITextField!
  @IBOutlet weak var passwordLabel: UILabel!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var forgotPasswordButton: UIButton!
  @IBOutlet weak var okButton: UIButton!
  @IBOutlet weak var facebookButton: UIButton!
  @IBOutlet weak var registerButton: UIButton!
  @IBOutlet weak var termsLabel: UILabel!
  @IBOutlet weak var registerScrollView: UIScrollView!
  @IBOutlet weak var avatarButton: UIButton!
  @IBOutlet weak var nameRegisterTextField: UITextField!
  @IBOutlet weak var emailRegisterTextField: UITextField!
  @IBOutlet weak var passwordRegisterTextField: UITextField!
  @IBOutlet weak var passwordConfirmationRegisterTextField: UITextField!
  @IBOutlet weak var logoImageViewCenterYConstraint: NSLayoutConstraint!
  @IBOutlet weak var nameRegisterTextFieldWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var termsWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var termsViewHeightConstraint: NSLayoutConstraint!
  
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  
  private var facebook = Facebook()
  
  var imagePicker: ImagePicker?
  var selectedAvatar: UIImage?
  var returningFromImagePicker = false
  var tap: UITapGestureRecognizer?
  var loginWithFacebook = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    avatarButton.layer.borderColor = UIColor(red: 254 / 255, green: 211 / 255, blue: 17 / 255, alpha: 1).CGColor
    bgView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    loginLabel.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    loginTextField.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    passwordLabel.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    passwordTextField.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    forgotPasswordButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    okButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    facebookButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    registerButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    registerScrollView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, view.bounds.height)
    nameRegisterTextFieldWidthConstraint.constant = UIScreen.mainScreen().bounds.width - 40 * 2
    termsWidthConstraint.constant = UIScreen.mainScreen().bounds.width - 64
    termsLabel.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 32 * 2
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: nil)
    keyboardNotificationObserver.installNotifications()
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    if returningFromImagePicker {
      returningFromImagePicker = false
      return
    }
    Async.main(after: 0.4) {
      self.logoImageViewCenterYConstraint.constant = 180
      self.backgroundStage(1)
      UIView.animateWithDuration(0.5, animations: { () -> Void in
        self.bgView.transform = CGAffineTransformIdentity
        if UIScreen.mainScreen().bounds.height == 480 {
          self.logoImageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.6, 0.6)
        } else {
          self.logoImageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9)
        }
        self.view.layoutIfNeeded()
        Async.main(after: 0.1) {
          UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
            self.loginLabel.transform = CGAffineTransformIdentity
            }, completion: nil)
        }
        Async.main(after: 0.2) {
          UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
            self.loginTextField.transform = CGAffineTransformIdentity
            }, completion: nil)
        }
        Async.main(after: 0.3) {
          UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
            self.passwordLabel.transform = CGAffineTransformIdentity
            }, completion: nil)
        }
        Async.main(after: 0.4) {
          UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
            self.passwordTextField.transform = CGAffineTransformIdentity
            }, completion: nil)
        }
        Async.main(after: 0.5) {
          UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
            self.forgotPasswordButton.transform = CGAffineTransformIdentity
            }, completion: nil)
        }
        Async.main(after: 0.6) {
          UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
            self.okButton.transform = CGAffineTransformIdentity
            }, completion: nil)
        }
        Async.main(after: 0.7) {
          UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
            self.facebookButton.transform = CGAffineTransformIdentity
            }, completion: nil)
        }
        Async.main(after: 0.8) {
          UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
            self.registerButton.transform = CGAffineTransformIdentity
            }, completion: nil)
        }
        }, completion: nil)
    }
    DDSAnalytics.trackScreen("Login", withContentId: nil)
  }
  
  // MARK: Facebook
  @IBAction func facebookTapped(sender: UIButton) {
    if !loadingView.isAnimating {
      loadingView.startAnimating()
      Async.main(after: 0.8) {
        self.facebook.loginWithFacebookFromViewController(self) {
          switch $0 {
          case .Success(let data):
            self.loginWithFacebook = true
            self.registerWithUserInfo(data, avatar: nil)
          case .Failure:
            break
          }
        }
      }
    }
  }
  
  private func backgroundStage(stage: Int) {
    let stageSize = (backgroundImageView.bounds.width - view.bounds.width) / 4
    UIView.animateWithDuration(1, delay: 0, options: .CurveEaseInOut, animations: { () -> Void in
      self.backgroundImageView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, CGFloat(-stage) * stageSize, 0)
      }, completion: nil)
  }
  
  // MARK: API
  func registerWithUserInfo(info: [String: AnyObject], avatar: UIImage?) {
    let completion = { (response: Response<AnyObject, NSError>) -> Void in
      self.blockUI(false)
      if self.loginWithFacebook {
        DDSAnalytics.trackLoginWithMethod("Facebook", success: response.result.isSuccess, customAttributes: nil)
      } else {
        DDSAnalytics.trackSignUpWithMethod("Email", success: response.result.isSuccess, customAttributes: nil)
      }
      switch response.result {
      case .Success(let data):
        if let JSON = data as? [String: AnyObject] {
          if let success = JSON["success"] as? Bool where success, let userAPI = JSON["user_data"] as? [String: AnyObject] {
            let user = User(userAPI, dateFormatter: nil)
            user.persist()
            if let avatarURL = userAPI["profile_image"] as? String {
              user.avatarURL = avatarURL
            } else if user.facebookId.characters.count > 0 {
              user.avatarURL = "http://graph.facebook.com/\(user.facebookId)/picture?type=large"
            }
            NetworkManager.Router.authToken = user.authToken
            self.loadingView.stopAnimating(true)
            Async.main(after: 0.8) {
              self.showTerms()
            }
          }
        } else {
          let alert = UIAlertController(title: "Erro desconhecido.", message: "Por favor, tente novamente.", preferredStyle: .Alert)
          alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { _ in
            self.loadingView.stopAnimating(true)
          }))
          self.presentViewController(alert, animated: true, completion: nil)
        }
      case .Failure(let error):
        let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { _ in
          self.loadingView.stopAnimating(true)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
      }
    }
    
    guard let avatar = avatar, avatarData = UIImagePNGRepresentation(avatar) else {
      request(NetworkManager.Router.CreateUser(info))
        .responseAPI(completion)
      return
    }
    
    let user = info["user"] as! [String: String]
    let email = user["email"]?.dataUsingEncoding(NSUTF8StringEncoding)
    let name = user["name"]?.dataUsingEncoding(NSUTF8StringEncoding)
    let password = user["password"]?.dataUsingEncoding(NSUTF8StringEncoding)
    let passwordConfirmation = user["password_confirmation"]?.dataUsingEncoding(NSUTF8StringEncoding)
    let surname = user["surname"]?.dataUsingEncoding(NSUTF8StringEncoding)
    let fbToken = user["fb_token"]?.dataUsingEncoding(NSUTF8StringEncoding)
    let fbId = user["fb_id"]?.dataUsingEncoding(NSUTF8StringEncoding)
    let deviceToken = user["device_token"]?.dataUsingEncoding(NSUTF8StringEncoding)
    let platform = user["platform"]?.dataUsingEncoding(NSUTF8StringEncoding)
  
    upload(NetworkManager.Router.CreateUser(nil), multipartFormData: { multipartFormData in
      if let email = email {
        multipartFormData.appendBodyPart(data: email, name: "user[email]")
      }
      if let name = name {
        multipartFormData.appendBodyPart(data: name, name: "user[name]")
      }
      if let password = password {
        multipartFormData.appendBodyPart(data: password, name: "user[password]")
      }
      if let passwordConfirmation = passwordConfirmation {
        multipartFormData.appendBodyPart(data: passwordConfirmation, name: "user[password_confirmation]")
      }
      if let surname = surname {
        multipartFormData.appendBodyPart(data: surname, name: "user[surname]")
      }
      if let fbToken = fbToken {
        multipartFormData.appendBodyPart(data: fbToken, name: "user[fb_token]")
      }
      if let fbId = fbId {
        multipartFormData.appendBodyPart(data: fbId, name: "user[fb_id]")
      }
      if let deviceToken = deviceToken {
        multipartFormData.appendBodyPart(data: deviceToken, name: "user[device_token]")
      }
      if let platform = platform {
        multipartFormData.appendBodyPart(data: platform, name: "user[platform]")
      }
      multipartFormData.appendBodyPart(data: avatarData, name: "user[profile_image]", fileName: "profile_image.png", mimeType: "image/png")
      }) { encodingResult in
        switch encodingResult {
        case .Success(let upload, _, _):
          upload.responseAPI(completion)
        case .Failure:
          let alert = UIAlertController(title: "Erro desconhecido.", message: "Por favor, tente novamente.", preferredStyle: .Alert)
          alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { _ in
            self.loadingView.stopAnimating(true)
          }))
          self.presentViewController(alert, animated: true, completion: nil)
        }
    }
  }
  
  @IBAction func loginTapped() {
    login()
  }
  
  func login() {
    if validateLogin() {
      Manager.sharedInstance.request(NetworkManager.Router.Login(loginTextField.text!, passwordTextField.text!))
        .responseAPI { response in
          DDSAnalytics.trackLoginWithMethod("Email", success: response.result.isSuccess, customAttributes: nil)
          switch response.result {
          case .Success(let data):
            if let JSON = data as? [String: AnyObject], success = JSON["success"] as? Bool where success, let userData = JSON["user_data"] as? [String: AnyObject] {
              let user = User(userData, dateFormatter: nil)
              user.persist()
              NetworkManager.Router.authToken = user.authToken
              self.loadingView.stopAnimating(true)
              if let deviceToken = PushNotification.persistedToken() {
                Manager.sharedInstance.request(NetworkManager.Router.EditProfile(["user": ["device_token": deviceToken, "platform": "ios"]]))
              }
              Async.main(after: 0.8) {
                DDSAnalytics.setUserId(String(user.id))
                DDSAnalytics.trackEvent("User Login", category: "UX", label: nil, value: nil, customAttributes: nil)
                (UIApplication.sharedApplication().delegate as! AppDelegate).goToHome()
              }
            }
          case .Failure(let error):
            let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { _ in
              self.loadingView.stopAnimating(true)
            }))
            self.presentViewController(alert, animated: true, completion: nil)
          }
      }
    }
  }
  
  func validateLogin() -> Bool {
    return loginTextField.text!.characters.count > 0 && passwordTextField.text!.characters.count > 0
  }
  
  // MARK: Forgot Password
  @IBAction func forgotPasswordTapped(sender: UIButton) {
    if loginTextField.text!.characters.count > 0 {
      Manager.sharedInstance.request(NetworkManager.Router.ForgotPassword(loginTextField.text!))
        .responseAPI { response in
          switch response.result {
          case .Success:
            self.presentSentAlert()
          case .Failure(let error):
            let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { _ in
              self.loadingView.stopAnimating(true)
            }))
            self.presentViewController(alert, animated: true, completion: nil)
          }
      }
    }
  }
  
  func presentSentAlert() {
    if let customAlert = NSBundle.mainBundle().loadNibNamed("CustomAlert", owner: self, options: nil).first as? CustomAlert {
      customAlert.configureForForgotPasswordAlert()
      customAlert.present()
    }
  }
  
  // MARK: Terms
  private func showFacebook() {
    termsViewHeightConstraint.constant = 0
    loginLabel.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    loginTextField.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    passwordLabel.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    passwordTextField.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    forgotPasswordButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    okButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    facebookButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    registerButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, view.bounds.width, 0)
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.view.layoutIfNeeded()
      }) { finished in
        self.backgroundStage(1)
        UIView.animateWithDuration(0.3, animations: { () -> Void in
          self.bgView.transform = CGAffineTransformIdentity
          self.registerScrollView.transform = CGAffineTransformIdentity
          Async.main(after: 0.1) {
            UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
              self.loginLabel.transform = CGAffineTransformIdentity
              }, completion: nil)
          }
          Async.main(after: 0.2) {
            UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
              self.loginTextField.transform = CGAffineTransformIdentity
              }, completion: nil)
          }
          Async.main(after: 0.3) {
            UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
              self.passwordLabel.transform = CGAffineTransformIdentity
              }, completion: nil)
          }
          Async.main(after: 0.4) {
            UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
              self.passwordTextField.transform = CGAffineTransformIdentity
              }, completion: nil)
          }
          Async.main(after: 0.5) {
            UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
              self.forgotPasswordButton.transform = CGAffineTransformIdentity
              }, completion: nil)
          }
          Async.main(after: 0.6) {
            UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
              self.okButton.transform = CGAffineTransformIdentity
              }, completion: nil)
          }
          Async.main(after: 0.7) {
            UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
              self.facebookButton.transform = CGAffineTransformIdentity
              }, completion: nil)
          }
          Async.main(after: 0.8) {
            UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseInOut, animations: {
              self.registerButton.transform = CGAffineTransformIdentity
              }, completion: nil)
          }
          }, completion: { finished in
            
        })
    }
  }
  
  private func showTerms() {
    UIView.animateWithDuration(0.3, animations: {
      self.bgView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, self.view.bounds.width, 0)
      self.registerScrollView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, self.view.bounds.width, 0)
      self.view.layoutIfNeeded()
      }) { finished in
        self.termsViewHeightConstraint.constant = self.view.bounds.height - 32 - 91 - 120 - 20
        self.backgroundStage(2)
        UIView.animateWithDuration(0.3, animations: {
          self.view.layoutIfNeeded()
          }, completion: nil)
    }
  }
  
  @IBAction func declineTerms(sender: UIButton) {
    User.removeUser()
    NetworkManager.Router.authToken = nil
    DDSAnalytics.trackEvent("Terms", category: "Legal", label: nil, value: 0, customAttributes: nil)
    showFacebook()
  }
  
  @IBAction func acceptTerms(sender: UIButton) {
    if let user = User.persistedUser() {
      
      DDSAnalytics.setUserId(String(user.id))
      DDSAnalytics.trackEvent("Terms", category: "Legal", label: nil, value: 1, customAttributes: nil)
    }
    (UIApplication.sharedApplication().delegate as! AppDelegate).goToHome()
  }
  
  // MARK: Register
  @IBAction func registerTapped() {
    presentRegisterView()
  }
  
  func presentRegisterView() {
    tap = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissRegister(_:)))
    backgroundImageView.addGestureRecognizer(tap!)
    registerScrollView.contentOffset = CGPoint.zero
    logoImageViewCenterYConstraint.constant = (UIScreen.mainScreen().bounds.height - logoImageView.bounds.height) / 2 - 10
    keyboardNotificationObserver.scrollView = registerScrollView
    UIView.animateWithDuration(0.5, delay: 0, options: .CurveEaseInOut, animations: {
      self.view.layoutIfNeeded()
      if UIScreen.mainScreen().bounds.height == 480 {
        self.logoImageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.6, 0.6)
      } else {
        self.logoImageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.7, 0.7)
      }
      self.registerScrollView.transform = CGAffineTransformIdentity
      }, completion: nil)
  }
  
  func dismissRegister(sender: UITapGestureRecognizer) {
    logoImageViewCenterYConstraint.constant = 180
    keyboardNotificationObserver.scrollView = nil
    UIView.animateWithDuration(0.5, delay: 0, options: .CurveEaseInOut, animations: {
      self.view.layoutIfNeeded()
      if UIScreen.mainScreen().bounds.height == 480 {
        self.logoImageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.6, 0.6)
      } else {
        self.logoImageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9)
      }
      self.registerScrollView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, self.view.bounds.height)
      }, completion: nil)
    backgroundImageView.removeGestureRecognizer(tap!)
    tap = nil
  }
  
  @IBAction func avatarTapped() {
    if imagePicker != nil {
      return
    }
    Async.main {
      self.returningFromImagePicker = true
      self.imagePicker = ImagePicker.presentOptionsInViewController(self) { image in
        if let image = image {
          self.avatarButton.setImage(image, forState: .Normal)
          self.avatarButton.imageView?.contentMode = .ScaleAspectFill
          self.selectedAvatar = image
        }
        self.imagePicker = nil
      }
    }
  }
  
  @IBAction func registerOkTapped() {
    if validateRegisterFields() {
      loadingView.startAnimating()
      blockUI(true)
      let name: String
      let lastName: String
      let components = nameRegisterTextField.text!.componentsSeparatedByString(" ")
      if components.count > 1 {
        name = components.first!
        lastName = components.last!
      } else {
        name = nameRegisterTextField.text!
        lastName = ""
      }
      var user = ["name": name, "surname": lastName, "email": emailRegisterTextField.text!, "password": passwordRegisterTextField.text!, "password_confirmation": passwordConfirmationRegisterTextField.text!]
      if let deviceToken = PushNotification.persistedToken() {
        user["device_token"] = deviceToken
        user["platform"] = "ios"
      }
      registerWithUserInfo(["user": user], avatar: selectedAvatar)
    }
  }
  
  func validateRegisterFields() -> Bool {
    return nameRegisterTextField.text!.characters.count > 0 && emailRegisterTextField.text!.characters.count > 0 && passwordRegisterTextField.text!.characters.count > 0 && passwordRegisterTextField.text! == passwordConfirmationRegisterTextField.text!
  }
  
  func blockUI(block: Bool) {
    navigationItem.backBarButtonItem?.enabled = !block
    nameRegisterTextField.userInteractionEnabled = !block
    emailRegisterTextField.userInteractionEnabled = !block
    passwordRegisterTextField.userInteractionEnabled = !block
    passwordConfirmationRegisterTextField.userInteractionEnabled = !block
    okButton.userInteractionEnabled = !block
  }
}
