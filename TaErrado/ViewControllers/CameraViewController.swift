//
//  CameraViewController.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/15/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import MapKit
import Async

final class CameraViewController: UIViewController {
  @IBOutlet weak var captureView: CaptureView!
  
  var aNavigationController: NavigationController!
  var imagePicker: ImagePicker?
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    captureView.addVideoInput()
    captureView.addStillImageOutput()
    captureView.addVideoPreviewLayer()
    captureView.captureSession.startRunning()
    showWarning()
    
    DDSAnalytics.trackScreen("Camera", withContentId: nil)
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    captureView.captureSession.stopRunning()
    captureView.removeVideoInput()
    captureView.removeStillImageOutput()
    captureView.removeVideoPreviewLayer()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if let pictureDetailsViewController = segue.destinationViewController as? PictureDetailsViewController, let offence = sender as? Offence {
      pictureDetailsViewController.offence = offence
      pictureDetailsViewController.aNavigationController = aNavigationController
    } else if let cropPictureViewController = segue.destinationViewController as? CropPictureViewController, let offence = sender as? Offence {
      cropPictureViewController.offence = offence
      cropPictureViewController.aNavigationController = aNavigationController
    }
  }
  
  // MARK: Dismiss
  @IBAction func dismiss(sender: UIButton) {
    aNavigationController.dismissCamera(nil)
  }
  
  // MARK: Advance
  func cropImage(image: UIImage, location: CLLocation) {
    let offence = Offence(image: image, location: location)
    performSegueWithIdentifier("SegueCrop", sender: offence)
  }
  
  func noLocation() {
    let customAlert = NSBundle.mainBundle().loadNibNamed("CustomAlert", owner: self, options: nil).first as! CustomAlert
    customAlert.present()
  }
  
  // MARK: Camera
  @IBAction func takePicture(sender: UIButton) {
    captureView.captureStillImage { image in
      if let image = image {
        if let location = self.aNavigationController.locationManager.location {
          let offence = Offence(image: image, location: location)
          Async.main {
            self.performSegueWithIdentifier("SeguePictureDetails", sender: offence)
          }
        } else {
          Async.main {
            self.noLocation()
          }
        }
      }
    }
  }
  
  @IBAction func invertCamera(sender: UIButton) {
    captureView.changeVideoInput()
  }
  
  @IBAction func gallery(sender: UIButton) {
    imagePicker = ImagePicker.presentInViewController(self) { image, location in
      if let image = image {
        if let location = location {
          self.cropImage(image, location: location)
        } else {
          self.noLocation()
        }
      }
      self.imagePicker = nil
    }
  }
  
  // MARK: Warning
  func showWarning() {
    if !NSUserDefaults.standardUserDefaults().boolForKey("WarningAlreadyPresented") {
      let customAlert = NSBundle.mainBundle().loadNibNamed("CustomAlert", owner: self, options: nil).first as! CustomAlert
      customAlert.configureForImageWarning()
      customAlert.present()
      NSUserDefaults.standardUserDefaults().setBool(true, forKey: "WarningAlreadyPresented")
      NSUserDefaults.standardUserDefaults().synchronize()
    }
  }
}
