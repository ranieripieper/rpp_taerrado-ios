//
//  CropPictureViewController.swift
//
//  Created by Gilson Gil on 3/23/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

final class CropPictureViewController: UIViewController {
  var cropView: CropView!
  var offence: Offence!
  var aNavigationController: NavigationController!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureView()
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    DDSAnalytics.trackScreen("Crop", withContentId: nil)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if let pictureDetailsViewController = segue.destinationViewController as? PictureDetailsViewController {
      pictureDetailsViewController.offence = offence
      pictureDetailsViewController.aNavigationController = aNavigationController
    }
  }
  
  // MARK: View
  func configureView() {
    cropView = CropView(image: offence.image!, andMaxSize: CGSize(width: UIScreen.mainScreen().bounds.width, height: UIScreen.mainScreen().bounds.height - 64 - 54))
    view.insertSubview(cropView, atIndex: 0)
    cropView.center = CGPoint(x: view.center.x, y: view.center.y + 5)
    cropView.imageView.layer.shadowColor = UIColor.blackColor().CGColor
    cropView.imageView.layer.shadowRadius = 3
    cropView.imageView.layer.shadowOpacity = 0.8
    cropView.imageView.layer.shadowOffset = CGSize(width: 1, height: 1)
    cropView.shouldChangeCropView = false
  }
  
  // MARK: Cancel
  @IBAction func cancelTapped(sender: UIButton) {
    navigationController?.popViewControllerAnimated(true)
  }
  
  // MARK: Done
  @IBAction func cropTapped(sender: UIButton) {
    offence.image = cropView.croppedImage()
    performSegueWithIdentifier("SeguePictureDetails", sender: nil)
  }
}
