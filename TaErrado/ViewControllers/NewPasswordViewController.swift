//
//  NewPasswordViewController.swift
//  TaErrado
//
//  Created by Gilson Gil on 7/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Alamofire

final class NewPasswordViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var passwordConfirmationTextField: UITextField!
  @IBOutlet weak var loadingView: LoadingView!
  @IBOutlet weak var textFieldDelegate: TextFieldDelegate!
  @IBOutlet weak var widthConstraint: NSLayoutConstraint!
  @IBOutlet weak var topSpaceConstraint: NSLayoutConstraint!
  
  var token: String!
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    widthConstraint.constant = UIScreen.mainScreen().bounds.width
    topSpaceConstraint.constant = UIScreen.mainScreen().bounds.height - 276
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    textFieldDelegate.returnCallback = { _ in
      self.okTapped()
    }
  }
  
  @IBAction func okTapped() {
    if validateFields() {
      loadingView.startAnimating()
      Manager.sharedInstance.request(NetworkManager.Router.NewPassword(token, passwordTextField.text!))
        .responseAPI { response in
          switch response.result {
          case .Success:
            (UIApplication.sharedApplication().delegate as! AppDelegate).goToLogin()
          case .Failure(let error):
            let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { _ in
              self.loadingView.stopAnimating(true)
            }))
            self.presentViewController(alert, animated: true, completion: nil)
          }
      }
    }
  }
  
  func validateFields() -> Bool {
    return token != nil && passwordTextField.text!.characters.count >= 6 && passwordTextField.text == passwordConfirmationTextField.text
  }
}
