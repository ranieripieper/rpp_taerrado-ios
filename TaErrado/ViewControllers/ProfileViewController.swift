//
//  ProfileViewController.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/17/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Alamofire
import Async

final class ProfileViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var profileBackgroundImageView: UIImageView!
  @IBOutlet weak var profileImageView: UIImageView!
  @IBOutlet weak var daysLabel: UILabel!
  @IBOutlet weak var picturesSentLabel: UILabel!
  @IBOutlet weak var swearingsLabel: UILabel!
  @IBOutlet weak var editButton: UIButton!
  @IBOutlet weak var headerBottomConstraint: NSLayoutConstraint!
  var adMobView: GADBannerView!
  @IBOutlet weak var feedCollectionDelegate: FeedCollectionDelegate!
  
  weak var aNavigationController: NavigationController? {
    return navigationController as? NavigationController
  }
  var userId: Int!
  var userImageURL: String?
  var offences = [Offence]()
  var currentPage = 0
  var loading = false
  var endReached = false
  var profileRequest: Request?
  var user: User?
  var noPostsLabel: UILabel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(UINib(nibName: "FeedCell", bundle: nil), forCellReuseIdentifier: "FeedCell")
    tableView.contentInset = UIEdgeInsets(top: 270, left: 0, bottom: 50, right: 0)
    tableView.estimatedRowHeight = 400
    tableView.rowHeight = UITableViewAutomaticDimension
    profileImageView.layer.borderColor = UIColor(red: 253 / 255, green: 211 / 255, blue: 17 / 255, alpha: 1).CGColor
    adMob()
    editButton.hidden = userId != nil
    
    loading = false
    endReached = false
    currentPage = 0
    getOffences()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    feedCollectionDelegate.delegate = self
    tableView.dataSource = feedCollectionDelegate
    tableView.delegate = feedCollectionDelegate
    
    if let user = user {
      fillProfile(user)
    }
    
    endReached = false
    currentPage = 0
    getOffences()
    tableView.contentOffset = CGPoint(x: 0, y: -334)
    
    DDSAnalytics.trackScreen("Profile", withContentId: nil)
  }
  
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    tableView.dataSource = nil
    tableView.delegate = nil
    feedCollectionDelegate.delegate = nil
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if let editProfileViewController = segue.destinationViewController as? EditProfileViewController {
      editProfileViewController.user = user
    }
  }
  
  // MARK: Profile
  func getOffences() {
    if loading || endReached {
      return
    }
    loading = true
    Offence.byUser(userId, page: currentPage + 1) {
      switch $0 {
      case .Success(let data):
        if let avatarURL = data.1?.avatarURL where avatarURL.characters.count > 0 {
          self.userImageURL = avatarURL
        } else if self.userImageURL == nil, let fbId = data.1?.facebookId {
          self.userImageURL = "http://graph.facebook.com/\(fbId)/picture?type=large"
        }
        let offences = data.0.filter() {
          $0.offendedImageURL = self.userImageURL ?? $0.offendedImageURL
          return true
        }
        self.endReached = offences.count == 0
        self.currentPage += 1
        if self.currentPage == 1 {
          self.offences = offences
        } else {
          self.offences += offences
        }
        if let user = data.1 {
          self.fillProfile(user)
        }
        self.tableView.reloadData()
        self.loading = false
        if self.offences.count == 0 {
          self.tableView.hidden = true
          self.noPostsLabel?.removeFromSuperview()
          self.noPostsLabel = nil
          self.noPostsLabel = UILabel(frame: CGRect(x: 0, y: 270, width: self.view.bounds.width, height: self.view.bounds.height - 270))
          self.noPostsLabel?.text = "Você não possui nenhum post ainda."
          self.noPostsLabel?.textColor = UIColor(red: 188 / 255, green: 188 / 255, blue: 188 / 255, alpha: 1)
          self.noPostsLabel?.textAlignment = .Center
          self.view.addSubview(self.noPostsLabel!)
        } else {
          self.tableView.hidden = false
          self.noPostsLabel?.removeFromSuperview()
          self.noPostsLabel = nil
        }
      case .Failure(let error):
        let alert = UIAlertController(title: error.localizedDescription, message: nil, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "ok", style: .Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
      }
    }
  }
  
  func fillProfile(user: User) {
    self.user = user
    daysLabel.text = user.createdAt.daysAgo()
    picturesSentLabel.text = String(user.picturesCount)
    swearingsLabel.text = String(user.swearings)
    
    let imageURL: String
    if let avatarURL = user.avatarURL where avatarURL.characters.count > 0 {
      imageURL = avatarURL
    } else {
      imageURL = "http://graph.facebook.com/\(user.facebookId)/picture?type=large"
    }
    
    let placeholderImage = UIImage(named: "img_placeholder")
    if let url = NSURL(string: imageURL) {
      profileImageView.pin_setImageFromURL(url, placeholderImage: placeholderImage) { result in
        self.profileImageView.image = result.image
        self.profileBackgroundImageView.image = result.image.profileBlurredImage()
      }
    }
  }
  
  // MARK: Menu
  func toggleMenu(sender: UIBarButtonItem) {
    aNavigationController?.toggleMenu(nil)
  }
  
  @IBAction func goToCamera(sender: UIBarButtonItem) {
    aNavigationController?.goToCamera()
  }
}

extension ProfileViewController { // Feed Cell Delegate
  func reportCell(cell: FeedCell) {
    let twoButtonAlert = NSBundle.mainBundle().loadNibNamed("TwoButtonAlert", owner: self, options: nil).first as! TwoButtonAlert
    twoButtonAlert.present()
    twoButtonAlert.callback = {
      if let indexPath = self.tableView.indexPathForCell(cell) {
        let offence = self.offences[indexPath.row]
        offence.report() {
          switch $0 {
          case .Success:
            offence.reported = true
            cell.updateReported(offence)
          case .Failure:
            break
          }
        }
      }
    }
  }
  
  func goToUserCell(cell: FeedCell) {
    
  }
  
  func swearCell(cell: FeedCell, on: Bool) {
    cell.swearButton.selected = !cell.swearButton.selected
    if let indexPath = self.tableView.indexPathForCell(cell) {
      let offence = self.offences[indexPath.row]
      if on {
        if offence.swearings == 0 {
          cell.swearingsLabel.text = "1 xingamento"
        } else {
          cell.swearingsLabel.text = "\(offence.swearings + 1) xingamentos"
        }
      } else {
        if offence.swearings == 2 {
          cell.swearingsLabel.text = "1 xingamento"
        } else {
          cell.swearingsLabel.text = "\(offence.swearings - 1) xingamentos"
        }
      }
      offence.sweared = on
      offence.swear(on) {
        switch $0 {
        case .Success(let data):
          offence.swearings = data
          offence.sweared = on
          cell.updateSwearings(offence)
        case .Failure:
          break
        }
      }
    }
  }
  
  func adMob() {
    Async.main(after: 1) {
      let bannerWidth: CGFloat = 320
      let bannerHeight: CGFloat = 50
      self.adMobView = GADBannerView(frame: CGRect(x: (UIScreen.mainScreen().bounds.width - bannerWidth) / 2, y: UIScreen.mainScreen().bounds.height - bannerHeight, width: bannerWidth, height: bannerHeight))
      self.adMobView.rootViewController = self
      self.adMobView.adUnitID = AdMob.unitId
      self.adMobView.loadRequest(AdMob.request())
      self.view.addSubview(self.adMobView)
      self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[ad(50)]-0-|", options: [], metrics: nil, views: ["ad": self.adMobView]))
      self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[ad(320)]", options: [], metrics: nil, views: ["ad": self.adMobView]))
      self.view.addConstraint(NSLayoutConstraint(item: self.adMobView, attribute: .CenterX, relatedBy: .Equal, toItem: self.view, attribute: .CenterX, multiplier: 1, constant: 0))
    }
  }
}

extension ProfileViewController: FeedCollectionDelegateDelegate {
  func didScroll(scrollView: UIScrollView) {
    headerBottomConstraint.constant = scrollView.contentOffset.y + view.bounds.height
  }
  
  func didReachEnd() {
    
  }
}
