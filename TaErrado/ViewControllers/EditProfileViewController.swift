//
//  EditProfileViewController.swift
//  TaErrado
//
//  Created by Gilson Gil on 7/10/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Alamofire
import Async

class EditProfileViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var avatarButton: UIButton!
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var saveButton: UIButton!
  @IBOutlet weak var deleteAccountButton: UIButton!
  @IBOutlet weak var loadingView: LoadingView!
  
  var adMobView: GADBannerView!
  var user: User?
  var keyboardNotificationObserver: KeyboardNotificationObserver!
  var imagePicker: ImagePicker?
  
  var newImage: UIImage?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    keyboardNotificationObserver = KeyboardNotificationObserver(scrollView: scrollView)
    keyboardNotificationObserver.installNotifications()
    avatarButton.layer.borderColor = UIColor(red: 253 / 255, green: 211 / 255, blue: 17 / 255, alpha: 1).CGColor
    let URLString: String
    if let user = user, avatarURL = user.avatarURL where avatarURL.characters.count > 0 {
      URLString = avatarURL
    } else if let avatarURL = NSUserDefaults.standardUserDefaults().objectForKey("H!*@(H#@N!&*T#&*GUI!") as? String {
      URLString = avatarURL
    } else {
      URLString = ""
    }
    request(.GET, URLString)
      .validate(contentType: ["image/*"])
      .responseImage { response in
        switch response.result {
        case .Success(let image):
          self.avatarButton.setImage(image, forState: .Normal)
        case .Failure:
          break
        }
    }
    avatarButton.imageView?.contentMode = .ScaleAspectFill
    adMob()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    DDSAnalytics.trackScreen("EditProfile", withContentId: nil)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    if let changePasswordViewController = segue.destinationViewController as? ChangePasswordViewController {
      changePasswordViewController.user = user
    }
  }
  
  // MARK: Avatar
  @IBAction func avatarTapped() {
    if imagePicker != nil {
      return
    }
    Async.main {
      self.imagePicker = ImagePicker.presentOptionsInViewController(self) { image in
        if let image = image {
          self.avatarButton.setImage(image, forState: .Normal)
        } else {
          request(.GET, self.user?.avatarURL ?? "")
            .validate(contentType: ["image/*"])
            .responseImage { response in
              switch response.result {
              case .Success(let image):
                self.avatarButton.setImage(image, forState: .Normal)
              case .Failure:
                break
              }
          }
        }
        self.avatarButton.imageView?.contentMode = .ScaleAspectFill
        self.newImage = image
        self.imagePicker = nil
      }
    }
  }
  
  // MARK: Delete Account
  @IBAction func deleteAccount(sender: UIButton) {
    if let twoButtonAlert = NSBundle.mainBundle().loadNibNamed("TwoButtonAlert", owner: self, options: nil).first as? TwoButtonAlert {
      twoButtonAlert.configureForDeleteAccount()
      twoButtonAlert.callback = deleteAccount
      twoButtonAlert.present()
    }
  }
  
  func deleteAccount() {
    loadingView.startAnimating()
    Manager.sharedInstance.request(NetworkManager.Router.DeleteAccount)
      .responseAPI { response in
        switch response.result {
        case .Success:
          self.loadingView.stopAnimating(true)
          Async.main(after: 0.2) {
            (UIApplication.sharedApplication().delegate as! AppDelegate).goToLogin()
          }
        case .Failure(let error):
          let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .Alert)
          alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { _ in
            self.loadingView.stopAnimating(true)
          }))
          self.presentViewController(alert, animated: true, completion: nil)
        }
    }
  }
  
  // MARK: Save
  @IBAction func saveTapped() {
    loadingView.startAnimating()
    var params: [String: AnyObject] = ["auth_token": NetworkManager.Router.authToken!]
    
    var user: [String: AnyObject] = [:]
    if let name = nameTextField.text where name.characters.count > 0 {
      user["name"] = name
    }
    if let email = emailTextField.text where email.characters.count > 0 {
      user["email"] = email
    }
    if let deviceToken = PushNotification.persistedToken() {
      user["device_token"] = deviceToken
      user["platform"] = "ios"
    }
    if user.count > 0 {
      params["user"] = user
    }
    
    let completion = { (response: Response<AnyObject, NSError>) -> Void in
      self.blockUI(false)
      switch response.result {
      case .Success(let data):
        if let JSON = data as? [String: AnyObject], let success = JSON["success"] as? Bool where success {
          if let userData = JSON["user"] as? [String: AnyObject], let newProfileImage = userData["profile_image_url"] as? String {
            self.user?.avatarURL = newProfileImage
          }
          self.navigationController?.popViewControllerAnimated(true)
        } else {
          let alert = UIAlertController(title: "Erro desconhecido", message: "Por favor, tente novamente.", preferredStyle: .Alert)
          alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { _ in
            self.loadingView.stopAnimating(true)
          }))
          self.presentViewController(alert, animated: true, completion: nil)
        }
      case .Failure(let error):
        let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { _ in
          self.loadingView.stopAnimating(true)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
      }
    }
    
    guard let avatar = self.newImage, avatarData = UIImagePNGRepresentation(avatar) else {
      request(NetworkManager.Router.EditProfile(params))
      .responseAPI(completion)
      return
    }
    
    guard let token = NetworkManager.Router.authToken, data = token.dataUsingEncoding(NSUTF8StringEncoding) else {
      
      return
    }
    
    let name = nameTextField.text?.dataUsingEncoding(NSUTF8StringEncoding)
    let email = emailTextField.text?.dataUsingEncoding(NSUTF8StringEncoding)
    let deviceToken = PushNotification.persistedToken()?.dataUsingEncoding(NSUTF8StringEncoding)
    let platform = "ios".dataUsingEncoding(NSUTF8StringEncoding)
    
    Manager.sharedInstance.upload(NetworkManager.Router.EditProfile(nil), multipartFormData: { (multipartFormData: MultipartFormData) -> Void in
      multipartFormData.appendBodyPart(data: avatarData, name: "user[profile_image]", fileName: "profile_image.png", mimeType: "image/png")
      multipartFormData.appendBodyPart(data: data, name: "auth_token")
      if let name = name {
        multipartFormData.appendBodyPart(data: name, name: "user[name]")
      }
      if let email = email {
        multipartFormData.appendBodyPart(data: email, name: "user[email]")
      }
      if let deviceToken = deviceToken {
        multipartFormData.appendBodyPart(data: deviceToken, name: "user[device_token]")
      }
      if let platform = platform {
        multipartFormData.appendBodyPart(data: platform, name: "user[platform]")
      }
      }) { encodingResult in
        switch encodingResult {
        case .Success(let upload, _, _):
          upload.responseAPI(completion)
        case .Failure:
          let alert = UIAlertController(title: "Erro desconhecido.", message: "Por favor, tente novamente.", preferredStyle: .Alert)
          alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { _ in
            self.loadingView.stopAnimating(true)
          }))
          self.presentViewController(alert, animated: true, completion: nil)
        }
    }
  }
  
  func blockUI(block: Bool) {
    avatarButton.userInteractionEnabled = !block
    nameTextField.userInteractionEnabled = !block
    emailTextField.userInteractionEnabled = !block
    passwordTextField.userInteractionEnabled = !block
    saveButton.userInteractionEnabled = !block
    deleteAccountButton.userInteractionEnabled = !block
    navigationController?.navigationItem.backBarButtonItem?.enabled = !block
  }
  
  func adMob() {
    Async.main(after: 1) {
      let bannerWidth: CGFloat = 320
      let bannerHeight: CGFloat = 50
      self.adMobView = GADBannerView(frame: CGRect(x: (UIScreen.mainScreen().bounds.width - bannerWidth) / 2, y: UIScreen.mainScreen().bounds.height - bannerHeight, width: bannerWidth, height: bannerHeight))
      self.adMobView.rootViewController = self
      self.adMobView.adUnitID = AdMob.unitId
      self.adMobView.loadRequest(AdMob.request())
      self.view.addSubview(self.adMobView)
      self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[ad(50)]-0-|", options: [], metrics: nil, views: ["ad": self.adMobView]))
      self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[ad(320)]", options: [], metrics: nil, views: ["ad": self.adMobView]))
      self.view.addConstraint(NSLayoutConstraint(item: self.adMobView, attribute: .CenterX, relatedBy: .Equal, toItem: self.view, attribute: .CenterX, multiplier: 1, constant: 0))
    }
  }
}

extension EditProfileViewController: UITextFieldDelegate {
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    performSegueWithIdentifier("SegueChangePassword", sender: nil)
    return false
  }
}
