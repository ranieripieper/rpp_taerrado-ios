//
//  AppDelegate.swift
//  TaErrado
//
//  Created by Gilson Gil on 5/15/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import JLRoutes
import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    Routes.addRoutes()
    Fabric.with([Crashlytics()])
    DDSAnalytics.setupWithTypes([.Answers, .GoogleAnalytics])
    Parse.setApplicationId("3IuyYTBsoMnKGbVxim7kJG5ZWHZdsiOGSOVV5URk", clientKey: "mWAFahzLuJVqDheD1bDOdIGbLh4s1NGo1NbpxmNA")
    PushNotification.registerForRemoteNotifications()
    if let user = User.persistedUser() {
      NetworkManager.Router.authToken = user.authToken
      if let remoteNotificationUserInfo = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? [String: AnyObject], userInfo = remoteNotificationUserInfo["aps"] as? [String: AnyObject] {
        JLRoutes.routeURL(Routes.PushRouteURL, withParameters: userInfo)
      } else {
        goToHome()
      }
    } else {
      goToLogin()
    }
    
    return true
  }
  
  func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
    if JLRoutes.globalRoutes().canRouteURL(url) {
      
      return JLRoutes.globalRoutes().routeURL(url)
    }
    
    return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
  }
  
  @available(iOS 9.0, *)
  func application(application: UIApplication, performActionForShortcutItem shortcutItem: UIApplicationShortcutItem, completionHandler: (Bool) -> Void) {
    let url = NSURL(string: shortcutItem.type)!
    let didHandle = JLRoutes.routeURL(url)
    
    completionHandler(didHandle)
  }
  
  func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
    PFPush.handlePush(userInfo)
    JLRoutes.routeURL(Routes.PushRouteURL, withParameters: userInfo)
    if application.applicationState == UIApplicationState.Inactive {
      PFAnalytics.trackAppOpenedWithRemoteNotificationPayload(userInfo)
    }
  }
  
  func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
    PushNotification.registerToken(deviceToken)
  }
  
  func goToLogin() {
    shortcutItemsEnabled(false)
    let loginViewController = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController() as! LoginViewController
    if let rootViewController = window?.rootViewController {
      let snapshot = rootViewController.view.snapshotViewAfterScreenUpdates(false)
      window?.rootViewController = loginViewController
      window?.addSubview(snapshot)
      UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseInOut, animations: {
        snapshot.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, self.window!.bounds.width, 0)
        }) { finished in
          snapshot.removeFromSuperview()
      }
    } else {
      window = UIWindow(frame: UIScreen.mainScreen().bounds)
      window?.makeKeyAndVisible()
      window?.rootViewController = loginViewController
    }
  }
  
  func goToNewPassword(token: String) {
    shortcutItemsEnabled(false)
    guard let newPasswordViewController = UIStoryboard(name: "Login", bundle: nil).instantiateViewControllerWithIdentifier("NewPasswordViewController") as? NewPasswordViewController else {
      return
    }
    if window == nil {
      window = UIWindow(frame: UIScreen.mainScreen().bounds)
      window?.makeKeyAndVisible()
    }
    newPasswordViewController.token = token
    window?.rootViewController = newPasswordViewController
  }
  
  func goToHome() {
    let navigationController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as! NavigationController
    shortcutItemsEnabled(true)
    if let rootViewController = window?.rootViewController {
      let snapshot = rootViewController.view.snapshotViewAfterScreenUpdates(false)
      window?.rootViewController = navigationController
      window?.addSubview(snapshot)
      navigationController.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, window!.bounds.width, 0)
      UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseInOut, animations: {
        snapshot.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -self.window!.bounds.width, 0)
        navigationController.view.transform = CGAffineTransformIdentity
        }) { finished in
          snapshot.removeFromSuperview()
      }
    } else {
      window = UIWindow(frame: UIScreen.mainScreen().bounds)
      window?.makeKeyAndVisible()
      window?.rootViewController = navigationController
    }
  }
  
  func goToProfile() {
    let navigationController: NavigationController
    if let rootViewController = window?.rootViewController as? NavigationController {
      navigationController = rootViewController
      rootViewController.profile = true
    } else {
      navigationController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as! NavigationController
      navigationController.profile = true
      window = UIWindow(frame: UIScreen.mainScreen().bounds)
      window?.makeKeyAndVisible()
      window?.rootViewController = navigationController
    }
    navigationController.goToProfile()
  }
  
  func shortcutItemsEnabled(enabled: Bool) {
    if #available(iOS 9.0, *) {
      let uploadShortcutItem = UIMutableApplicationShortcutItem(type: "com.doisdoissete.TaErrado.UploadApplicationShortcut", localizedTitle: "Postar nova infração", localizedSubtitle: nil, icon: UIApplicationShortcutIcon(templateImageName: "icn_new_post"), userInfo: nil)
      let shortcutItems = [uploadShortcutItem]
      
      UIApplication.sharedApplication().shortcutItems = enabled ? shortcutItems : []
    }
  }
}
