//
//  ObjcBridging.h
//  TaErrado
//
//  Created by Gilson Gil on 5/15/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

#ifndef TaErrado_ObjcBridging_h
#define TaErrado_ObjcBridging_h

#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import <GoogleAnalytics/GAIFields.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "UIImage+ImageEffects.h"
#import <JLRoutes/JLRoutes.h>
//#import "UIImageView+PINRemoteImage.h"
#import <GoogleMobileAds/GoogleMobileAds.h>

#endif
